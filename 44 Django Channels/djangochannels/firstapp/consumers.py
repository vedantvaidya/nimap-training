from channels.consumer import SyncConsumer, AsyncConsumer
from channels.exceptions import StopConsumer
from asgiref.sync import async_to_sync
from .models import Group, Chat
import json
from channels.db import database_sync_to_async

class MySyncConsumer(SyncConsumer):
    def websocket_connect(self, event):
        print("Websocket Connected...", event)
        print(
            "Channel Layer...", self.channel_layer
        )  # get default channel layer from a project
        print("Channel Name...", self.channel_name)  # get channel Name
        #  add a channel to a new or existing group
        async_to_sync(self.channel_layer.group_add)(
            "programmers", self.channel_name  # group name
        )
        self.send({"type": "websocket.accept"})

    def websocket_receive(self, event):
        print("Message Received from Client...", event["text"])
        print("Type of Message Received from Client...", type(event["text"]))
        async_to_sync(self.channel_layer.group_send)(
            "programmers", {"type": "chat.message", "message": event["text"]}
        )

    def chat_message(self, event):
        print("Event...", event)
        print("Actual Data...", event["message"])
        print("Type of Actual Data...", type(event["message"]))
        self.send({"type": "websocket.send", "text": event["message"]})

    def websocket_disconnect(self, event):
        print("Websocket Disconnected...", event)
        print(
            "Channel Layer...", self.channel_layer
        )  # get default channel layer from a project
        print("Channel Name...", self.channel_name)  # get channel Name
        async_to_sync(self.channel_layer.group_discard)(
            "programmers", self.channel_name
        )
        raise StopConsumer()


class MyAsyncConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        self.room_token = self.scope["url_route"]["kwargs"]["room_token"]
        print(self.room_token)
        group = await self.get_or_create_group()
        chat = await self.get_chat_messages(group)
        # if group:
        #     chat = Chat.objects.filter(group=group)
        # else:
        #     group = Group(name = self.room_token)
        #     group.save()
        await self.channel_layer.group_add(self.room_token, self.channel_name)
        await self.send({"type": "websocket.accept"})

    async def websocket_receive(self, event):
        data=json.loads(event['text'])
        group = await self.get_or_create_group()
        await self.save_chat_message(group,data['msg'])
        await self.channel_layer.group_send(
            self.room_token, {"type": "chat.message", "message": event["text"]}
        )

    async def chat_message(self, event):
        await self.send({"type": "websocket.send", "text": event["message"]})

    async def websocket_disconnect(self, event):
        await self.channel_layer.group_discard(self.room_token, self.channel_name)
        raise StopConsumer()

    @database_sync_to_async
    def get_or_create_group(self):
        group, _ = Group.objects.get_or_create(name=self.room_token)
        return group
    
    @database_sync_to_async
    def get_chat_messages(self, group):
        return list(Chat.objects.filter(group=group).values())
    
    @database_sync_to_async
    def save_chat_message(self, group, message):
        Chat.objects.create(group=group, message=message)