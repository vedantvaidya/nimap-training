from django.urls import path
from . import consumers

websocket_urlpatterns = [
    path('ws/sc', consumers.MySyncConsumer.as_asgi()),
    path('ws/ac/<str:room_token>', consumers.MyAsyncConsumer.as_asgi()),
]