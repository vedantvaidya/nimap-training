from django.db import models

class Chat(models.Model):
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    group = models.ForeignKey("Group", on_delete=models.CASCADE)

    class Meta:
        db_table = "CHAT_MESSAGE"

    def __str__(self):
        return self.message
    
class Group(models.Model):
    name = models.CharField(max_length=255)