from cryptography.fernet import Fernet

# Generate a key
key = b'ZWCwSe8TjROFGn7Z8ZjcTErxXVyeQYvu6cgq9V2Oi4s='


cipher = Fernet(key)




def encrypt(text):
    return cipher.encrypt(text.encode())

def decrypt(text):
    return cipher.decrypt(text).decode()

ans = b'gAAAAABmJjyHiCoI474tyxVn_1deRzlsmizhiP3GBI7zHQ1KJUWCmEzQPk1W-pPGQDKCG3pccAJrgW1SlQLeN9bJS63UF9Qw5Q=='

while True:
    a=input("Give an example of pig?")
    if a.lower().strip()==decrypt(ans).lower().strip():
        print(f"Correct {decrypt(ans)}")
        break
    else:
        print("Nah...")