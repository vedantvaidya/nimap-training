from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password
from .form import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.paginator import Paginator
from django.views.decorators.cache import cache_page
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt

# * Success
# ! Danger
# ? Information
# TODO: Task


@cache_page(30)
def home(request):
    """Home Page.
    It will be cached for 30 seconds
    """
    if request.user.is_authenticated:
        return redirect('/tasklist')
    return render(request,"index.html")

def handelsignup(request):
    """ Responsible to handel signup activity"""
    if request.method=="GET":
        return render(request,"register.html")
        
    elif request.method=="POST":
        try:
            name=request.POST.get('name')
            password=request.POST.get('password')
            password = make_password(password)
            user_instance=User(username=name,password=password)
            user_instance.save()
            messages.success(request,"Account Created Successfully")
            return redirect('/')
        except:
            messages.success(request,"Invalid Details")
            return redirect('/signup')
@csrf_exempt    
def handellogin(request):
    """ Responsible to handel login activity"""
    if request.method=="GET":
        return render(request,"login.html")

    if request.method=="POST":
        name=request.POST.get('name')
        pswd=request.POST.get('password')
        print(name,pswd)

        user=authenticate(request,username=name,password=pswd)
        if user is not None:
            request.session["uid"]=user.id
            login(request,user)
            messages.success(request,"Logged In Successfully")
            return redirect("/tasklist")
        else:
            messages.success(request,"Invalid Details")
            return redirect("/login")
        
@login_required(login_url="/login")
def handellogout(request):
    """ Responsible to handel logout activity"""
    logout(request)
    messages.success(request,"Logged Out Successfully")
    return redirect("/")



def tasklist(request):
    if request.user.is_authenticated:
        id=request.user.id
        all_tasks=Task.objects.filter(owner_id=id)
        task_paginator=Paginator(all_tasks,10)
        page_number=request.GET.get("page")
        page=task_paginator.get_page(page_number)
        dict={"task":page}
        return render(request,"tasklist.html",dict)
    else:
        messages.success(request,"Permission Denied. Please Login")
        return redirect('/login')


@login_required(login_url="/login")
def createtask(request):
    if request.method=="GET":
        return render(request, "createtask.html")
    elif request.method=="POST":
        id=request.session.get("uid")
        user_instance=User.objects.get(id=id)
        task_name=request.POST.get("task_name")
        status=int(request.POST.get("status"))
        if task_name=="" or status not in [0,1]:
            messages.success(request,"Invalid Details")
            return redirect("/createtask")
        task_instance=Task()
        task_instance.task_name=task_name
        task_instance.status=status
        task_instance.owner_id=user_instance
        task_instance.save()
        return redirect('/tasklist')
    

@login_required(login_url="/login")
def task(request,pk):
    if request.method=="GET":
        task_instance=Task.objects.get(id=pk)
        subtask_instances=subtask.objects.filter(task_id=task_instance.id)
        dict={"task":task_instance,"subtask":subtask_instances}
        return render(request, "updatetask.html",dict)
    if request.method=="POST":
        id=request.session.get("uid")
        task_name=request.POST.get("task_name")
        status=int(request.POST.get("status"))
        task_instance=Task.objects.get(id=pk)
        task_instance.task_name=task_name
        task_instance.status=status
        task_instance.save()
        return redirect("/tasklist")

@login_required(login_url="/login")
def addsubtask(request,pk):
    task_instance=Task.objects.get(id=pk)
    subtask_name=request.POST.get("subtask_name")
    subtask_instance=subtask()
    subtask_instance.subtask_name=subtask_name
    subtask_instance.task_id=task_instance
    subtask_instance.save()
    # return redirect('/tasklist')
    return redirect('/task/'+str(pk))