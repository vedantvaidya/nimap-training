from django.shortcuts import render

def home(request):
    context={"heading":"<i>Auto Escape</i>","data" : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],}
    return render(request,"index.html",context)