from django.urls import path
from . import views

urlpatterns=[
    path('',views.home, name="home"),
    path('handelsignup',views.handelsignup),
    path('handellogin',views.handellogin),
    path('handellogout',views.handellogout),
]