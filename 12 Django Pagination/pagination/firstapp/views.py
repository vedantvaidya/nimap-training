from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Pokemon
from django.core.paginator import Paginator
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required

@login_required(login_url="/handellogin")
def home(request):
    pokemons=Pokemon.objects.all()
    pokemon_paginator=Paginator(pokemons,10)
    page_number=request.GET.get("page")
    page=pokemon_paginator.get_page(page_number)
    dict={"page":page}
    return render(request,"index.html",dict)


def handelsignup(request):
    if request.method=="GET":
        form=CreateUserForm()
        context={'form':form}
        return render(request,"signup.html", context)
    
    elif request.method=="POST":
        form=CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,"Account Created Successfully")
            return redirect('/')
        messages.success(request,"Invalid Details")
        return redirect("/handelsignup")
    else:
        messages.success(request,"Invalid Request")
        return redirect(request,"/handelsignup")

def handellogin(request):
    if request.user.is_authenticated:
        return redirect("/")
    else:
        if request.method=="GET":
            return render(request,"login.html")
        
        if request.method=="POST":
            username=request.POST.get("username")
            password=request.POST.get("password")
            user= authenticate(request, username=username, password=password)
            if user is not None:
                login(request,user)
                messages.success(request,"Login Success")
                return redirect("/")
            messages.success(request,"Invalid Details")
            return redirect("/handellogin")
    

def handellogout(request):
    logout(request)
    messages.success(request,"Logged Out Successfully")
    return redirect("/handellogin")