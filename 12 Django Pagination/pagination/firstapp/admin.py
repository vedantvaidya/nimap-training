from django.contrib import admin
from .models import *


class PokemonAdmin(admin.ModelAdmin):
    list_display=['id','pokemon_name']
admin.site.register(Pokemon,PokemonAdmin)