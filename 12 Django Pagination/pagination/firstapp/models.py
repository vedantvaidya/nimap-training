from django.db import models

class Pokemon(models.Model):
    pokemon_name=models.CharField(max_length=20)

    def __str__(self):
        return self.pokemon_name