from .models import Student
from .serializers import StudentSerializer
from rest_framework import viewsets

class StudentModelViewSet(viewsets.ModelViewSet):
# class StudentReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
# Use this to keep only read apis
  queryset = Student.objects.all()
  serializer_class = StudentSerializer