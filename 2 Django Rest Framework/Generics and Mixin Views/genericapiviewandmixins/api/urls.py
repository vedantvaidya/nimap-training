from django.urls import path
from . import views

urlpatterns=[
    path('studentapi/',views.StudentListCreate.as_view()),
    path('studentapi/<int:pk>/',views.StudentRetrieveUpdateDelete.as_view()),
    path('test/',views.testing),
]