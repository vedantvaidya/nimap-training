from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Student
from django.http import JsonResponse
from .serializer import StudentSerializer
# from django.views.decorators.csrf import csrf_exempt
from rest_framework import status



@api_view(['GET','POST'])
def student_create_list(request):

    if request.method=="GET":
        student_instance=Student.objects.all()
        serializer=StudentSerializer(student_instance, many=True)
        return Response(serializer.data)
    

    if request.method=="POST":
        new_instance=request.data
        print(request.data)
        serializer=StudentSerializer(data=new_instance)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"Added Successfully"})
        return Response({"msg":"Invalid detauils"})
    
@api_view(["GET","PUT","DELETE","PATCH"])
def student_update_delete_get(request,id):
    try:
        student_instance=Student.objects.get(id=id)
    except Student.DoesNotExist:
        return Response({"Detail":"Record Doesnot Exisist"},status=status.HTTP_404_NOT_FOUND)
    if request.method == "GET":
        serializer=StudentSerializer(student_instance)
        return Response(serializer.data)
    if request.method == "PUT" or request.method=='PATCH':
        new_instance=request.data
        serializer=StudentSerializer(student_instance,data=new_instance,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"Updated Successfully"})
        return Response({"msg":"Invalid Details"})
    if request.method == "DELETE":
        student_instance.delete()
        return Response({"msg":"Deleted Successfully"})
    