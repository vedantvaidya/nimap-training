from django.urls import path
from . import views

urlpatterns=[
    path('studentapi/',views.student_create_list),
    path('studentapi/<int:id>/',views.student_update_delete_get),
]