from django.urls import path
from . import views

urlpatterns=[
    path('studentapi/',views.StudentAPI.as_view()),
    path('studentapi/<int:id>/',views.StudentAPI.as_view()),
]