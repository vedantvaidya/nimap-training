from django.shortcuts import render
from rest_framework.response import Response
from .models import Student
from django.http import JsonResponse
from .serializer import StudentSerializer
# from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.views import APIView

class StudentAPI(APIView):
    def get(self,request,id=None, format=None):
        if id!=None:
            try:
                student_instance=Student.objects.get(id=id)
            except Student.DoesNotExist:
                return Response({"Detail":"Record Doesnot Exisist"},status=status.HTTP_404_NOT_FOUND)
            serializer=StudentSerializer(student_instance)
            return Response(serializer.data)
        student_instance=Student.objects.all()
        serializer=StudentSerializer(student_instance, many=True)
        return Response(serializer.data)

    def post(self,request, format=None):
        new_instance=request.data
        print(request.data)
        serializer=StudentSerializer(data=new_instance)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"Added Successfully"})
        return Response({"msg":"Invalid detauils"})
    
    def put(self,request, id, format=None):
        try:
            student_instance=Student.objects.get(id=id)
        except Student.DoesNotExist:
            return Response({"Detail":"Record Doesnot Exisist"},status=status.HTTP_404_NOT_FOUND)
        new_instance=request.data
        serializer=StudentSerializer(student_instance,data=new_instance,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"Updated Successfully"})
        return Response({"msg":"Invalid Details"})
    
    def patch(self,request, id, format=None):
        try:
            student_instance=Student.objects.get(id=id)
        except Student.DoesNotExist:
            return Response({"Detail":"Record Doesnot Exisist"},status=status.HTTP_404_NOT_FOUND)
        new_instance=request.data
        serializer=StudentSerializer(student_instance,data=new_instance,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"Updated Successfully"})
        return Response({"msg":"Invalid Details"})
    
    def delete(self,request, id, format=None):
        try:
            student_instance=Student.objects.get(id=id)
        except Student.DoesNotExist:
            return Response({"Detail":"Record Doesnot Exisist"},status=status.HTTP_404_NOT_FOUND)
        student_instance.delete()
        return Response({"msg":"Deleted Successfully"})


