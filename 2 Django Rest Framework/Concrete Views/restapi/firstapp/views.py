from django.shortcuts import render
from rest_framework import generics,viewsets
from .models import Userinfo
from .serializer import UISerializer


class CreateUser(generics.CreateAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer

class ListUser(generics.ListAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer

class UpdateUser(generics.UpdateAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer

class DeleteUser(generics.DestroyAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer


class GetUser(generics.RetrieveAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer



class CreateListUser(generics.ListCreateAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer


class UpdateDeleteGet(generics.RetrieveUpdateDestroyAPIView):
    queryset=Userinfo.objects.all()
    serializer_class=UISerializer