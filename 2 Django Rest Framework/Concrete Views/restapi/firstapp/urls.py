from django.urls import path
from . import views as v


urlpatterns=[
    path("createuser/",v.CreateUser.as_view()),
    path("listuser/",v.ListUser.as_view()),
    path("updateuser/<int:pk>/",v.UpdateUser.as_view()),
    path("deleteuser/<int:pk>/",v.DeleteUser.as_view()),
    path("getuser/<int:pk>/",v.GetUser.as_view()),
    
    path("createlistuser/",v.CreateListUser.as_view()),
    path("updatedeleteget/<int:pk>/",v.UpdateDeleteGet.as_view()),
]