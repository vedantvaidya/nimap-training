from rest_framework import serializers
from .models import Userinfo

class UISerializer(serializers.ModelSerializer):
    class Meta:
        model=Userinfo
        fields="__all__"
        read_only_fields=['age']