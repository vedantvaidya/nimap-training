from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from .models import Student
from .serializer import StudentSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import io
from django.views.decorators.csrf import csrf_exempt


def student_detail(request,pk):
    try:
        student = Student.objects.get(id=pk)
    except Student.DoesNotExist:
        return JsonResponse({"detail":"Record does not exist."},safe=False)
    serializer=StudentSerializer(student)
    json_data=JSONRenderer().render(serializer.data)
    return HttpResponse(json_data,content_type='application/json')

@csrf_exempt
def student_api(request):
    if request.method=="GET":
        obj=Student.objects.all()
        serialized=StudentSerializer(obj, many=True)
        return JsonResponse(serialized.data,safe=False)
    
    if request.method=="POST":
        json_data=request.body
        stream=io.BytesIO(json_data)
        pythondata=JSONParser().parse(stream)
        serializer=StudentSerializer(data=pythondata)
        if serializer.is_valid():
            serializer.save()
            res={"msg":"Data Created"}
            json_data=JSONRenderer().render(res)
            return HttpResponse(json_data,content_type="application/json")     
        json_data=JSONRenderer().render(serializer.errors)
        return HttpResponse(json_data,content_type="application/json")
    
    if request.method=="PUT":
        json_data=request.body
        stream=io.BytesIO(json_data)
        pythondata=JSONParser().parse(stream)
        pk=pythondata.get("id",None)
        try:
            student_instance=Student.objects.get(id=pk)
        except Student.DoesNotExist:
            return JsonResponse({"detail":"Record does not exist."},safe=False)
        serializer=StudentSerializer(student_instance,data=pythondata, partial=True)
        if serializer.is_valid():
            serializer.save()
            res={"msg":"Data Updates"}
            json_data=JSONRenderer().render(res)
            return HttpResponse(json_data,content_type="application/json")
        json_data=JSONRenderer().render(serializer.errors)
        return HttpResponse(json_data,content_type="application/json")

    if request.method=="DELETE":
        json_data=request.body
        stream=io.BytesIO(json_data)
        pythondata=JSONParser().parse(stream)
        pk=pythondata.get("id",None)
        try:
            student_instance=Student.objects.get(id=pk)
        except Student.DoesNotExist:
            return JsonResponse({"detail":"Record does not exist."},safe=False)
        student_instance.delete()
        res={"msg":"Data Deleted"}
        json_data=JSONRenderer().render(res)
        return HttpResponse(json_data,content_type="application/json")


    