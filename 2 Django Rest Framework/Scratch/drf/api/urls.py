from django.contrib import admin
from django.urls import path,include
from api import views 

urlpatterns = [
    
    path('student/',views.student_api),
    path('student/<int:pk>/',views.student_detail),
    # path('addinfo/',views.add_detail),    
    # path('updateinfo/<int:pk>/',views.update_detail),    
    # path('deleteinfo/<int:pk>/',views.delete_detail),    
]