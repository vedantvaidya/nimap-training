from django.contrib import admin
from django.urls import path,include
from api2 import views 

urlpatterns = [
    
    path('teacher/',views.TeacherAPI.as_view()),
    path('teacher/<int:pk>/',views.teacher_detail),  
]