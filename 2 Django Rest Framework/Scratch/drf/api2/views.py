from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from .models import Teacher
from .serializer import TeacherSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import io
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View


@method_decorator(csrf_exempt,name="dispatch")
class TeacherAPI(View):
    def get(self,request,*args,**kwargs):
        obj=Teacher.objects.all()
        serialized=TeacherSerializer(obj, many=True)
        return JsonResponse(serialized.data,safe=False)
    
    def post(self,request,*args,**kwargs):
        json_data=request.body
        print(json_data)
        stream=io.BytesIO(json_data)
        pythondata=JSONParser().parse(stream)
        serializer=TeacherSerializer(data=pythondata)
        if serializer.is_valid():
            serializer.save()
            res={"msg":"Data Created"}
            json_data=JSONRenderer().render(res)
            return HttpResponse(json_data,content_type="application/json")     
        json_data=JSONRenderer().render(serializer.errors)
        return HttpResponse(json_data,content_type="application/json")
    
    def put(self,request,*args,**kwargs):
        json_data=request.body
        stream=io.BytesIO(json_data)
        pythondata=JSONParser().parse(stream)
        pk=pythondata.get("id",None)
        try:
            teacher_instance=Teacher.objects.get(id=pk)
        except Teacher.DoesNotExist:
            return JsonResponse({"detail":"Record does not exist."},safe=False)
        serializer=TeacherSerializer(teacher_instance,data=pythondata, partial=True)
        if serializer.is_valid():
            serializer.save()
            res={"msg":"Data Updates"}
            json_data=JSONRenderer().render(res)
            return HttpResponse(json_data,content_type="application/json")
        json_data=JSONRenderer().render(serializer.errors)
        return HttpResponse(json_data,content_type="application/json")

    def delete(self,request,*args,**kwargs):
        json_data=request.body
        stream=io.BytesIO(json_data)
        pythondata=JSONParser().parse(stream)
        pk=pythondata.get("id",None)
        try:
            teacher_instance=Teacher.objects.get(id=pk)
        except Teacher.DoesNotExist:
            return JsonResponse({"detail":"Record does not exist."},safe=False)
        teacher_instance.delete()
        res={"msg":"Data Deleted"}
        json_data=JSONRenderer().render(res)
        return HttpResponse(json_data,content_type="application/json")




def teacher_detail(request,pk):
    try:
        teacher = Teacher.objects.get(id=pk)
    except Teacher.DoesNotExist:
        return JsonResponse({"detail":"Record does not exist."},safe=False)
    serializer=TeacherSerializer(teacher)
    json_data=JSONRenderer().render(serializer.data)
    return HttpResponse(json_data,content_type='application/json')

# @csrf_exempt
# def teacher_api(request):
#     if request.method=="GET":
#         obj=Teacher.objects.all()
#         serialized=TeacherSerializer(obj, many=True)
#         return JsonResponse(serialized.data,safe=False)
    
#     if request.method=="POST":
#         json_data=request.body
#         stream=io.BytesIO(json_data)
#         pythondata=JSONParser().parse(stream)
#         serializer=TeacherSerializer(data=pythondata)
#         if serializer.is_valid():
#             serializer.save()
#             res={"msg":"Data Created"}
#             json_data=JSONRenderer().render(res)
#             return HttpResponse(json_data,content_type="application/json")     
#         json_data=JSONRenderer().render(serializer.errors)
#         return HttpResponse(json_data,content_type="application/json")
    
#     if request.method=="PUT" or request.method=="PATCH":
#         json_data=request.body
#         stream=io.BytesIO(json_data)
#         pythondata=JSONParser().parse(stream)
#         pk=pythondata.get("id",None)
#         try:
#             teacher_instance=Teacher.objects.get(id=pk)
#         except Teacher.DoesNotExist:
#             return JsonResponse({"detail":"Record does not exist."},safe=False)
#         serializer=TeacherSerializer(teacher_instance,data=pythondata, partial=True)
#         if serializer.is_valid():
#             serializer.save()
#             res={"msg":"Data Updates"}
#             json_data=JSONRenderer().render(res)
#             return HttpResponse(json_data,content_type="application/json")
#         json_data=JSONRenderer().render(serializer.errors)
#         return HttpResponse(json_data,content_type="application/json")

#     if request.method=="DELETE":
#         json_data=request.body
#         stream=io.BytesIO(json_data)
#         pythondata=JSONParser().parse(stream)
#         pk=pythondata.get("id",None)
#         try:
#             teacher_instance=Teacher.objects.get(id=pk)
#         except Teacher.DoesNotExist:
#             return JsonResponse({"detail":"Record does not exist."},safe=False)
#         teacher_instance.delete()
#         res={"msg":"Data Deleted"}
#         json_data=JSONRenderer().render(res)
#         return HttpResponse(json_data,content_type="application/json")


    