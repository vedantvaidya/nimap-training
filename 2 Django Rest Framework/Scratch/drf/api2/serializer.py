from rest_framework import serializers
from .models import Teacher
# Validators (You need to call this)
def starts_with_x(value):
    if value.startswith("X"):
        raise serializers.ValidationError("Invalid Name")
    
class TeacherSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100, validators=[starts_with_x])
    city=serializers.CharField(max_length=100)

    def create(self,validate_data):
        return Teacher.objects.create(**validate_data)

    def update(self,instance,validated_data):
        instance.name=validated_data.get('name', instance.name)
        instance.city=validated_data.get('city', instance.city)
        instance.save()
        return instance
    



    # # This is field level validator
    # def validate_name(self,value):
    #     if value.startswith("X"):
    #         raise serializers.ValidationError("Invalid Name")
    #     return value


    # # This is object level validation
    # def validate(self,data):
    #     name=data.get('name')
    #     city=data.get("city")
    #     if name.startswith("X") or city.startswith("X"):
    #         raise serializers.ValidationError("InvalidDetails")
    #     return data


     