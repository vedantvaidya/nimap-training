import asyncio
import websockets

class connections:
    users=[]

async def echo(websocket, path):
    connections.users.append(websocket)
    for user in connections.users:
        async for message in websocket:
            await user.send(message)


# Start the WebSocket server
start_server = websockets.serve(echo, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
