# import cv2
# import mediapipe as mp

# mp_hands = mp.solutions.hands
# hands = mp_hands.Hands()

# cap = cv2.VideoCapture(0)
# cap.set(3, 640)
# cap.set(4, 480)

# while cap.isOpened():
#     ret, frame = cap.read()
#     if not ret:
#         break

#     rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

#     results = hands.process(rgb_frame)

#     if results.multi_hand_landmarks:
#         for hand_idx, landmarks in enumerate(results.multi_hand_landmarks):
#             print(f"Hand {hand_idx + 1} landmarks:")
#             for idx, landmark in enumerate(landmarks.landmark):
#                 print(f"Landmark {idx}: {landmark.x}, {landmark.y}, {landmark.z}")

#     cv2.imshow("Hand Gesture Detection", frame)

#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

# cap.release()
# cv2.destroyAllWindows()


import cv2
import mediapipe as mp
from ctypes import cast, POINTER
from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume

mp_hands = mp.solutions.hands
hands = mp_hands.Hands()

cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)

devices = AudioUtilities.GetSpeakers()
interface = devices.Activate(
    IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
volume = cast(interface, POINTER(IAudioEndpointVolume))

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break

    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    results = hands.process(rgb_frame)

    if results.multi_hand_landmarks:
        for hand_idx, landmarks in enumerate(results.multi_hand_landmarks):
            # Assuming you're using a webcam, adjust these values based on your camera's resolution
            hand_bottom = landmarks.landmark[4].y
            hand_top = landmarks.landmark[0].y

            # Map hand movement to volume control (adjust these thresholds accordingly)
            if hand_top < hand_bottom - 0.05:  # Moving hand from bottom to top
                volume.SetMasterVolumeLevelScalar(volume.GetMasterVolumeLevelScalar() + 0.01, None)
                print("Increasing Volume")
            elif hand_bottom < hand_top - 0.05:  # Moving hand from top to bottom
                volume.SetMasterVolumeLevelScalar(volume.GetMasterVolumeLevelScalar() - 0.01, None)
                print("Decreasing Volume")

    cv2.imshow("Hand Gesture Detection", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
