from django.db import models
from django.contrib.auth.models import AbstractUser
from core.models import TimestampModel
from authentication.manager import UserManager

class Auditor(AbstractUser,TimestampModel):
    username = None
    email = models.EmailField(unique=True)
    fmno = models.IntegerField(null=True,blank=True)
    mobile_no = models.CharField(max_length = 20,null=True,blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objexts = UserManager()

    class Meta:
        db_table="Auditor"