from django.urls import path
from . import views

urlpatterns=[
    path("list",views.BookList.as_view(),name='book-list')
]