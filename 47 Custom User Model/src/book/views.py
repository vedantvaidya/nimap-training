from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView
from .models import Book
from.serializers import BookSerializer
from authentication.permissions import GroupPermission

class BookList(ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = [GroupPermission]
    app_model = ["book", "book"]