import requests
from bs4 import BeautifulSoup

with open("data/test.html" , "r") as f:
    html_doc = f.read()

soup = BeautifulSoup(html_doc, "html.parser")
# # Shows all code formatted
# print(soup.prettify())

# # Shows title tag
# print(soup.title) 
# print(soup.title.string, type(soup.title.string))

# # Find all div Tags
# print(soup.find_all("div")[0])

# # Find all links and their texts
# for link in soup.find_all("a"):
#     print(link.get("href"))
#     print(link.get_text())