from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
from django.template.response import TemplateResponse
def home(request):
    print("I am View")
    return JsonResponse({"I am sending JSON Responce":"Because its cool"})


def exc(request):
    print("I am Exception View")
    a = 10/0
    return HttpResponse("This is Exception page")

def user_info(request):
    print("I am User Info View")
    context = {"name":"Vedant"}
    return TemplateResponse(request, "user.html", context)