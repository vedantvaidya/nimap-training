from django.urls import path
from. import views

urlpatterns=[
    path("",views.home),
    path("exc",views.exc),
    path("user",views.user_info),
]