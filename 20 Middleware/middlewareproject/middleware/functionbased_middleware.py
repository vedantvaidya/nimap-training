def my_middleware(get_response):
    print("One Time Initialization Function Based Middleware")
    
    
    def middleware_function(request):
        
        print("This is before view Function Based Middleware")
        response = get_response(request)

        print("This is after view Function Based Middleware")
        return response

    return middleware_function