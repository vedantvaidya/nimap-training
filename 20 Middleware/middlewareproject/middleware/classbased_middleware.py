from django.http import HttpResponse

class MyMiddleware:

    def __init__(self,get_response):
        self.get_response=get_response
        print("One Time Initialization Class Based Middleware")
    
    
    def __call__(self,request):
        print("This is before view Class Based Middleware")
        response = self.get_response(request)
        print("This is after view Class Based Middleware")
        return response


    # def process_view(request,*args,**kwargs):
    #     print("This is Process View - Before view")
    #     # return HttpResponse("This is comming from process_view. View is not hitted")
    #     return None

    def process_exception(self,request,exception):
        print("Exception Occured")
        return HttpResponse(exception)
    
    def process_template_response(self,request,response):
        print("Process Template Response")
        response.context_data['name']="Arfa"
        return response
