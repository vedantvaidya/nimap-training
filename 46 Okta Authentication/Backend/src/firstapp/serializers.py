from rest_framework import serializers


class OktaTokenSerializer(serializers.Serializer):
    okta_token=serializers.CharField()