from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView
from rest_framework_simplejwt.tokens import RefreshToken
import requests
from django.contrib.auth.models import User
from firstapp.serializers import OktaTokenSerializer


class OktaTokenExchangeView(CreateAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = OktaTokenSerializer

    def create(self, request, *args, **kwargs):
        okta_token = request.data.get('okta_token')
        # id_token = "eyJraWQiOiJJbU9YQzkzNlRGWGl0dnduNExlemRvVjhnMVFSSHhNNGRTelV4Z25YMTM4IiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULlJ6d2d3RDlGek1kMHV6cTZRV0dvQW1uVjRPWTdva3BscjJ5OFN2QlBVaGciLCJpc3MiOiJodHRwczovL2Rldi00MTM5MTg2MS5va3RhLmNvbS9vYXV0aDIvZGVmYXVsdCIsImF1ZCI6ImFwaTovL2RlZmF1bHQiLCJpYXQiOjE3MTY0OTIwNjIsImV4cCI6MTcxNjQ5NTY2MiwiY2lkIjoiMG9haDlkNHMxdXFEeEdsS3k1ZDciLCJ1aWQiOiIwMHVoN3liNHM2d05PVWdXazVkNyIsInNjcCI6WyJwcm9maWxlIiwib3BlbmlkIiwiZW1haWwiXSwiYXV0aF90aW1lIjoxNzE2NDkyMDYwLCJzdWIiOiJ2ZWRhbnRzZHZhaWR5YUBnbWFpbC5jb20ifQ.V7bwCj102FRYsy9JFKYpcc7FUQeAaW84MXWrSsmrptMPVXDYuhLpf3Wi6yl3DRtC6p5hEs5tnpiPyTzj1W7TNydmRjnq6mBjJMYylV_Cda7EPJwYV-vcdWIBwGLDeT0cOI16usRf-FPWTtYYP1n1vdr-7pER63UXb1L6QkmhR49Q1KTfqlAegICJK-PFzSKJJDfLpPqVmWJMBpaahwJWf1jgnN8fxuBvPdL0X072DvJEp0kiSQXbhMIfUwbwNABWRkGoDJPCn_SDVn5aXlvPla9IUZNpNm_ZJbJobNJH3NGeQec7GOcq906O558WtMJsJKpBHPZK6_jVmqp326Nekw"
        if not okta_token:
            return Response({"error": "Okta ID token is required"}, status=status.HTTP_400_BAD_REQUEST)

        okta_domain = 'https://dev-41391861.okta.com'
        headers = {
            'Authorization': f'Bearer {okta_token}'
        }
        try:
            response = requests.get(f'{okta_domain}/oauth2/default/v1/userinfo',headers=headers)
            response_data = response.json()
            if not response_data.get('email_verified'):
                return Response({"error": "Invalid ID token"}, status=status.HTTP_400_BAD_REQUEST)
            email = response_data.get('email')

            user, _ = User.objects.get_or_create(username=email)

            print(_)

            refresh = RefreshToken.for_user(user)
            return Response({
                'refresh': str(refresh),
                'access': str(refresh.access_token),
            })
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
