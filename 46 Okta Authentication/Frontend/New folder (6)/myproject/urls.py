# myproject/urls.py

from django.contrib import admin
from django.urls import path, include
from myapp.views import OIDCAuthenticationView
from myapp.api_views import ProtectedAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('authorization-code/callback/', OIDCAuthenticationView.as_view(), name='oidc_authentication'),
    path('api/protected/', ProtectedAPIView.as_view(), name='protected_api'),
]
