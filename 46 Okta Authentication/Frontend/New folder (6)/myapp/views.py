

#=============================================testing for redirect_url==============================
import secrets
import string
from datetime import datetime, timedelta
from django.conf import settings
from django.shortcuts import redirect
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from django.contrib.auth.models import User
from oauth2_provider.models import AccessToken
import requests

# Function to generate a random token
def generate_token(length=40):
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for _ in range(length))

class OIDCAuthenticationView(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        authorization_url = settings.OIDC_OP_AUTHORIZATION_ENDPOINT
        client_id = settings.OIDC_RP_CLIENT_ID
        # This redirect URI should be your backend endpoint that handles the callback
        redirect_uri = request.build_absolute_uri('/authorization-code/callback')
        scope = 'openid profile email'
        state = generate_token()  # Use a proper state value to protect against CSRF attacks

        if 'code' not in request.GET:
            url = f'{authorization_url}?response_type=code&client_id={client_id}&redirect_uri={redirect_uri}&scope={scope}&state={state}'
            return redirect(url)
        else:
            code = request.GET.get('code')
            token_endpoint = settings.OIDC_RP_TOKEN_ENDPOINT
            client_id = settings.OIDC_RP_CLIENT_ID
            client_secret = settings.OIDC_RP_CLIENT_SECRET

            data = {
                'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': redirect_uri,
                'client_id': client_id,
                'client_secret': client_secret,
            }

            response = requests.post(token_endpoint, data=data)
            token_data = response.json()
            access_token = token_data.get('access_token')
            

            # Fetch user info
            userinfo_endpoint = settings.OIDC_OP_USER_ENDPOINT
            headers = {'Authorization': f'Bearer {access_token}'}
            userinfo_response = requests.get(userinfo_endpoint, headers=headers)
            userinfo = userinfo_response.json()

            # Create or update user
            user, _ = User.objects.update_or_create(
                username=userinfo['sub'],
                defaults={
                    'email': userinfo.get('email', ''),
                    'first_name': userinfo.get('given_name', ''),
                    'last_name': userinfo.get('family_name', '')
                }
            )
            print(userinfo)
            

            # Calculate token expiration time
            expires_in = token_data['expires_in']
            expires = datetime.now() + timedelta(seconds=expires_in)

            # Generate a unique access token
            access_token_value = generate_token()

            # Create access token for the user
            access_token = AccessToken.objects.create(
                user=user,
                token=access_token_value,
                scope='',
                expires=expires
            )

            # Redirect to your specified URL with access token information
            redirect_url = f'http://maudit-2-react-env-env.us-east-1.elasticbeanstalk.com:3000/?access_token={access_token.token}&token_type=Bearer&expires_in={expires_in}'
            return redirect(redirect_url)


