import React from 'react';
import OktaConfig from '@okta/okta-react';
import OktaSignIn from '@okta/okta-signin-widget';

const config = {
  orgUrl: 'https://dev-41391861.okta.com',  // Replace with your Okta organization URL
  clientId: '0oah9d4s1uqDxGlKy5d7',          // Replace with your Client ID from Okta app
  redirectUri: 'http://localhost:3000/callback', // Replace with your redirect URI
};

function Login() {
  return (
    <div>
      <OktaConfig config={config}>
        <OktaSignIn />
      </OktaConfig>
    </div>
  );
}

export default Login;
