import React from 'react';
import Login from './Login'; // Import the Login component

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Login />  {/* Render the Login component */}
      </header>
    </div>
  );
}

export default App;
