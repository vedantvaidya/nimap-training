from django.shortcuts import render
from django.http import JsonResponse
from .models import Fruits
from django.core.cache import cache

def home(request):
    payload=[]
    if cache.get("allfruits"):
        fruits=cache.get("allfruits")
        msg="From Redis Cache"
    else:
        fruits=Fruits.objects.all()
        cache.set("allfruits",fruits,30)
        msg="From Database"
    for i in fruits:
        payload.append(i.name)
    return JsonResponse({"fruits":payload,"msg":msg})