from django.shortcuts import render,redirect
from .models import *
from django.http import HttpResponse,JsonResponse
from django.db.models import Count
import datetime

def home(request):
    return render(request,"index.html")

def filter_query(request):
    Person_instance=Person.objects.filter(name__startswith="A")
    print(Person_instance.query)
    return HttpResponse(Person_instance)

def exclude_query(request):
    Person_instance=Person.objects.exclude(id=1)
    return HttpResponse(Person_instance)

def orderby_query(request):
    Person_instance=Person.objects.order_by('name')
    return HttpResponse(Person_instance)

def reverse_query(request):
    Person_instance=Person.objects.order_by('id').reverse()
    return HttpResponse(Person_instance)

def values_query(request):
    Person_instance=Person.objects.all().values()
    return HttpResponse(Person_instance)


def valueslist_query(request):
    Person_instance=Person.objects.all().values_list()
    return HttpResponse(Person_instance)

def all_query(request):
    Person_instance=Person.objects.all()
    return HttpResponse(Person_instance)

def get_query(request):
    Person_instance=Person.objects.get(id__exact=1)
    return HttpResponse(Person_instance)

def create_query(request):
    intrest=request.GET.get("intrest")
    Intrests_instance=Intrests.objects.create(title=intrest)
    return HttpResponse("Intrest Added")

def getorcreate_query(request):
    city=request.GET.get("city")
    city_instance,created=City.objects.get_or_create(title=city)
    if created:
        return HttpResponse("New City Added")
    else:
        return HttpResponse(city_instance)
    
def updateorcreate_query(request):
    mobile=request.GET.get("mobile")
    name=request.GET.get("name")
    person_instance,created=Person.objects.update_or_create(name=name, defaults={"mobile":mobile})
    if created:
        return HttpResponse("New Person Created")
    return HttpResponse(person_instance)


def bulkcreate_query(request):
    user_intrests=request.GET.get("intrests")
    user_intrests=user_intrests.split(",")
    list_of_objects=[]
    for i in user_intrests:
        a=Intrests(title=i)
        list_of_objects.append(a)
    Intrests.objects.bulk_create(list_of_objects)
    return render(request,"index.html")

def bulkupdate_query(request):
    person_list=[]
    person=Person.objects.filter(id__in=[7,4])
    for i in person:
        i.mobile=request.GET.get("number")
        person_list.append(i)
    query=Person.objects.bulk_update(person_list,fields=['mobile'])
    return render(request,"index.html")

def annotate_query(request):
    address=PersonalAddress.objects.annotate(count=Count('city')).select_related("person")
    for i in address:
        print(i.person,i.count)
    return render(request,"index.html")

def aggregate_query(request):
    address=PersonalAddress.objects.aggregate(count=Count('city'))
    print(address)
    return render(request,"index.html")

# Annotate gives extracolumn whereas aggregate only gives the value that we specifies
# eg:
# annonate
# {
#     name="Vedant"
#     lastname="Vaidya"
#     fullname = "Vedant Vaidya"
# }

# aggregate
# {
#     count=7
# }



def count_query(request):
    persons=Person.objects.all().count()
    return HttpResponse(persons)

def latest_query(request):
    persons=Person.objects.all().latest("id")
    return HttpResponse(persons)

def earliest_query(request):
    persons=Person.objects.all().earliest("id")
    return HttpResponse(persons)

def first_query(request):
    persons=Person.objects.all().first()
    return HttpResponse(persons)

def last_query(request):
    persons=Person.objects.all().last()
    return HttpResponse(persons)


def exists_query(request):
    person=Person.objects.filter(name="Vedant")
    if person.exists():
        return HttpResponse("Vedant Exist")
    else:
        return HttpResponse("Vedant Doesnt Exist")
    
def update_query(request):
    person=Person.objects.filter(name="Vedant").update(mobile="0987654321")
    return HttpResponse("Updated")

def delete_query(request):
    person=Person.objects.filter(id=7).delete()
    return HttpResponse(person)

def selectrelated_query(request):
    personal_address=PersonalAddress.objects.select_related().all()
    cities=[i.city for i in personal_address]
    return HttpResponse(cities)

def prefetchedrelated_query(request):
    pass

def contains_lookups(request):
    person=Person.objects.filter(name__contains="A")
    name=[i.name for i in person]
    return HttpResponse(name)


def icontains_lookups(request):
    person=Person.objects.filter(name__icontains="V")
    name=[i.name for i in person]
    return HttpResponse(name)


def in_lookups(request):
    person=Person.objects.filter(id__in=[1,3])
    name=[i.name for i in person]
    return HttpResponse(name)


def gt_lookups(request):
    person=Person.objects.filter(id__gt=3)
    name=[i.name for i in person]
    return HttpResponse(name)


def gte_lookups(request):
    person=Person.objects.filter(account_created__year__gte=2021)
    name=[i.name for i in person]
    return HttpResponse(name)


def lt_lookups(request):
    person=Person.objects.filter(account_created__year__lt=2024)
    name=[i.name for i in person]
    return HttpResponse(name)


def lte_lookups(request):
    person=Person.objects.filter(account_created__year__lte=2023)
    name=[i.name for i in person]
    return HttpResponse(name)


def startswith_lookups(request):
    person=Person.objects.filter(name__startswith="r")
    name=[i.name for i in person]
    return HttpResponse(name)

def istartswith_lookups(request):
    person=Person.objects.filter(name__istartswith="r")
    name=[i.name for i in person]
    return HttpResponse(name)


def endswith_lookups(request):
    person=Person.objects.filter(name__endswith="t")
    name=[i.name for i in person]
    return HttpResponse(name)


def iendswith_lookups(request):
    person=Person.objects.filter(name__endswith="T")
    name=[i.name for i in person]
    return HttpResponse(name)


def range_lookups(request):
    person=Person.objects.filter(id__range=(2,4))
    name=[i.name for i in person]
    return HttpResponse(name)


def date_lookups(request):
    person=Person.objects.filter(account_created__date=datetime.date(2023,5,18))
    name=[i.name for i in person]
    return HttpResponse(name)


def year_lookups(request):
    person=Person.objects.filter(account_created__year=2023)
    name=[i.name for i in person]
    return HttpResponse(name)


