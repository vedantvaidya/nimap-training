from django.contrib import admin
from .models import Student, Marks

class StudentAdmin(admin.ModelAdmin):
    list_display=['id','name','roll','city']
admin.site.register(Student,StudentAdmin)

class MarksAdmin(admin.ModelAdmin):
    list_display=['python','java','sql','student']
admin.site.register(Marks,MarksAdmin)