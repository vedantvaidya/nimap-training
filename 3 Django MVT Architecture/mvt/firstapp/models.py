from django.db import models

class Student(models.Model):
    name=models.CharField(max_length=100)
    roll=models.IntegerField()
    city=models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Marks(models.Model):
    python=models.IntegerField()
    java=models.IntegerField()
    sql=models.IntegerField()
    student=models.ForeignKey(Student,on_delete=models.CASCADE)