from celery import shared_task
from django.contrib.auth.models import User
from django.core.mail import send_mail
from sendmail import settings


@shared_task(bind=True)
def send_mail_func(self):
    user = User.objects.all()
    for i in user:
        mail_subject = "Hi Celery Testing"
        mail_message = f"Hello {i.username}"
        to_email = i.email
        print(i.email)
        send_mail(
            subject=mail_subject,
            message=mail_message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[to_email],
            fail_silently=False,
        )
    return "Mail Sent"

