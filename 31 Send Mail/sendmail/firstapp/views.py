from django.shortcuts import render
from django.http import HttpResponse
from .task import send_mail_func

def home(request):
    send_mail_func.delay()
    return HttpResponse("Hello")