from reactpy import html, component, run


@component
def header():
    return html.h1("Todo List")


@component
def task_list():
    return html.ul(
        html.li(html.b("Eat")),
        html.li(html.a({"style": {"href": "www.youtube.com"}}, "Code")),
        html.li("Sleep"),
        html.li("Repeat"),
    )


@component
def test_ccomponent():
    return html.div(
        {
            "style": {
                "width": "50%",
                "margin_left": "25%",
            },
        },
        html.h1(
            {
                "style": {
                    "margin_top": "0px",
                },
            },
            "My Todo List",
        ),
        html.ul(
            html.li("Build a cool new app"),
            html.li("Share it with the world!"),
        ),
    )

@component
def HeadComponent():
    return html.head(
        html.meta({"style": {"charset":"utf-8"}}),
        html.meta({"style": {"httpEquiv":"X-UA-Compatible", "content":"IE=edge"}}),
        html.title("WhatsApp Automation 2.0"),
        html.meta({"style": {"name":"description","content":""}}),
        html.meta({"style": {"name":"viewport", "content":"width=device-width", "initial_scale":"1"}}),
        # html.script({"type": "text/javascript", "src": "/eel.js"}),
        html.link({"style": {"rel":"stylesheet", "href":"bootstrap.min.css"}}),
        # html.link(rel="stylesheet", href="main.css"),
        # html.script({"src": "//code.jquery.com/jquery-1.12.0.min.js"}),
        # html.script({"src": "//code.jquery.com/jquery-migrate-1.2.1.min.js"})
    )
@component
def app():
    return html.section(
        HeadComponent(),
        header(),
        task_list(),
        test_ccomponent()
        )


run(app)
