from django.shortcuts import render
from ics import Calendar, Event
from datetime import datetime
from django.core.mail import EmailMessage
from icsproject import settings


def index(request):
    event = Event()
    event.name = "Event Title"
    event.begin = datetime(2023, 8, 19, 10, 0, 0)
    event.end = datetime(2023, 8, 19, 12, 0, 0)
    event.description = "Event Description"
    cal = Calendar()
    cal.events.add(event)
    ics_data = str(cal)
    email = EmailMessage(
        subject="Hi Whatsup",
        body="Does it work?",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=["vedantsdvaidya@gmail.com"],
    )
    email.attach("event.ics", ics_data, "text/calendar")
    email.send()
