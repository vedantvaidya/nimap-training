from django.urls import path
from . import views

urlpatterns=[
    path("",views.home),
    path("signup",views.handelsignup),
    path("login",views.handellogin),
    path("tasklist",views.tasklist),
    path("logout",views.handellogout),
    path("createtask",views.createtask),
    path("task/<int:pk>",views.task),
    path("addsubtask/<int:pk>/",views.addsubtask, name="add_subtask"),
]