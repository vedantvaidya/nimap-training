from django.urls import path
from firstapp import views
from rest_framework_simplejwt.views import (TokenObtainPairView,TokenRefreshView,TokenVerifyView)

urlpatterns = [
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path("list",views.BookListCreateAPIView.as_view(),name="book-list"),
    path("list/<int:pk>",views.BookRetrieveUpdateDestroyAPIView.as_view(),name="book-list")
]