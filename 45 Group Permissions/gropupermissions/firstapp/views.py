from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from firstapp.models import Book
from firstapp.serializers import BookSerializer
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from firstapp.permissions import GroupPermission

class BookListCreateAPIView(ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    app_model = ["firstapp", "book"]
    authentication_classes=[JWTAuthentication]
    permission_classes=[GroupPermission]

class BookRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    app_model = ["firstapp", "book"]
    authentication_classes=[JWTAuthentication]
    permission_classes=[GroupPermission]