from django.db import models

class Book(models.Model):
    name=models.CharField(max_length=100)
    pages=models.IntegerField()

    def __str__(self):
        return self.name
    

class AuditWorkingFile(models.Model):
    name=models.CharField(max_length=100)
    pages=models.IntegerField()

    class Meta:
        managed=False
        db_table = 'audit_working_file'
        # permissions = [
        #     ("view_auditworkingfile", "Can view Audit Working File"),
        #     ("change_auditworkingfile", "Can change Audit Working File"),
        #     ("add_auditworkingfile", "Can add Audit Working File"),
        #     ("delete_auditworkingfile", "Can delete Audit Working File"),
            
        # ]

    def __str__(self):
        return self.name
    

