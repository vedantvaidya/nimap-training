from django.shortcuts import render
from .serializers import SttudentSerializer
from rest_framework.generics import ListAPIView
from .models import Student
from django_filters.rest_framework import DjangoFilterBackend


class StudentList(ListAPIView):
    queryset=Student.objects.all()
    serializer_class=SttudentSerializer
    filter_backends=[DjangoFilterBackend]
    filterset_fields=['passedby']