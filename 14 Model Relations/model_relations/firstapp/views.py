from django.shortcuts import render
from .models import *

def home(request):
    return render(request,"index.html")

def selectrelated(request):
    # books=Book.objects.all()
    # for i in books:
    #     print(i.title)
    #     print(i.author)
    # Here one query is usedto get the title of allbooks. But toget the author(Forein Key) each timeaquery is fired

    books=Book.objects.select_related("author")
    for i in books:
        print(i.title)
        print(i.author)
    # Here only one query will be fired which will be like this
    # SELECT * FROM "firstapp_book" INNER JOIN "firstapp_author" ON ("firstapp_book"."author_id" = "firstapp_author"."id")


    context={"books":books}
    return render(request,"selectrelated.html",context)

def prefetchrelated(request):
    # books=Book.objects.prefetch_related("author")
    # for i in books:
    #     print(i.title)

    authors =Author.objects.prefetch_related("book_set")
    for author in authors:
        print(author.name)
        for book in author.book_set.all():
            print(book.title)
    context={"author":author}
    return render(request,"prefetchrelated.html",context)