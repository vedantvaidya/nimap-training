from django.db import models


class Publisher(models.Model):
    name=models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Author(models.Model):
    name=models.CharField(max_length=50)
    age=models.IntegerField()
    publisher=models.ForeignKey(Publisher,on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Book(models.Model):
    title=models.CharField(max_length=30)
    pages=models.IntegerField()
    author=models.ForeignKey(Author,on_delete=models.CASCADE)

    def __str__(self):
        return self.title