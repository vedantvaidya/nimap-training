from django.contrib import admin
from .models import *

class BookAdmin(admin.ModelAdmin):
    list_display=['id','title',"pages",'author']
admin.site.register(Book,BookAdmin)

class AuthorAdmin(admin.ModelAdmin):
    list_display=['id','name',"age",'publisher']
admin.site.register(Author,AuthorAdmin)


class PublisherAdmin(admin.ModelAdmin):
    list_display=['id','name']
admin.site.register(Publisher,PublisherAdmin)