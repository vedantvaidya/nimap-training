from django.shortcuts import render
from .serializers import SttudentSerializer
from rest_framework.generics import ListAPIView
from .models import Student
from rest_framework.filters import SearchFilter



class StudentList(ListAPIView):
    queryset=Student.objects.all()
    serializer_class=SttudentSerializer
    filter_backends=[SearchFilter]
    search_fields=['name',"passedby"]
