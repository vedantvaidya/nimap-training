from django.urls import path
from . import views

urlpatterns=[
    path("",views.home),
    path("quotes",views.quote_create_list),
    path("quotes/<int:id>",views.quote_get_update_delete),
    path("categories",views.CategorieAPI.as_view()),
    path("categories/<int:id>",views.CategorieAPI.as_view()),
    path("products",views.ProductAPI.as_view()),
    path("products/<int:id>",views.ProductAPI.as_view()),
    path("faq",views.FaqAPI),
]