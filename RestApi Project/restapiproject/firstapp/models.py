from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator



class Quote(models.Model):
    text=models.CharField(max_length=200)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)

class Categorie(models.Model):
    title=models.CharField(max_length=200)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class Product(models.Model):
    title=models.CharField(max_length=200)
    category=models.ForeignKey(Categorie,null=True,on_delete=models.SET_NULL,related_name="belonging_category")
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


class FAQ(models.Model):
    question=models.CharField(max_length=200)
    answer=models.CharField(max_length=200)
    attachment=models.FileField(blank=True, validators=[FileExtensionValidator(allowed_extensions=["pdf"])])

class Request_Log(models.Model):
    user_id=models.IntegerField(null=True)
    request_method=models.CharField(max_length=10)
    request_path=models.CharField(max_length=500)
    response_status=models.IntegerField()
    remote_address=models.CharField(max_length=200)
    server_hostname=models.CharField(max_length=200)
    request_body=models.JSONField(null=True)
    run_time=models.DecimalField(max_digits=23,decimal_places=20)
    timestamp=models.DateTimeField(auto_now=True)
