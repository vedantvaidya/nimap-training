from django.contrib import admin
from .models import Quote,Categorie,Product,FAQ, Request_Log


@admin.register(Quote)
class QuoteAdmin(admin.ModelAdmin):
    list_display=['id','text','created_at','updated_at']

@admin.register(Categorie)
class CategoryAdmin(admin.ModelAdmin):
    list_display=['id','title','created_at','updated_at']

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display=['id','title','category','created_at','updated_at']

@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display=['id','question','answer','attachment']


@admin.register(Request_Log)
class Request_LogAdmin(admin.ModelAdmin):
    list_display=['id','user_id','request_method','request_path','response_status','remote_address','server_hostname','request_body','run_time','timestamp']


