from rest_framework import serializers
from .models import Quote, Categorie, Product, FAQ

class QuoteSerializer(serializers.ModelSerializer):
    class Meta:
        model=Quote
        fields="__all__"

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model=Product
        fields=['id','title','created_at','updated_at','category']
        depth=1


class CategorieSerializer(serializers.ModelSerializer):
    belonging_category=ProductSerializer(many=True, read_only=True)
    class Meta:
        model=Categorie
        fields=['id','title','created_at','updated_at','belonging_category']

class FAQSerializer(serializers.ModelSerializer):
    class Meta:
        model=FAQ
        fields="__all__"