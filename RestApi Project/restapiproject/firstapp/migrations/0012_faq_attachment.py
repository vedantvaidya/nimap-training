# Generated by Django 4.2.1 on 2023-06-19 10:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstapp', '0011_remove_faq_attachment'),
    ]

    operations = [
        migrations.AddField(
            model_name='faq',
            name='attachment',
            field=models.FileField(blank=True, upload_to=''),
        ),
    ]
