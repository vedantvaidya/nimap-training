from django.http import HttpResponse
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import (
    CreateModelMixin,
    DestroyModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from .models import Categorie, Product, Quote,  FAQ
from .mypagination import MyPagePagination
from .serializer import CategorieSerializer, ProductSerializer, QuoteSerializer,FAQSerializer
from rest_framework.permissions import IsAuthenticated, IsAdminUser, DjangoModelPermissions, DjangoModelPermissionsOrAnonReadOnly
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from django.core.cache import cache
from django.http import JsonResponse


def home(request):
    return render(request, "first.html")


@extend_schema(request=QuoteSerializer,responses=QuoteSerializer)
@api_view(["GET", "POST"])
def quote_create_list(request):
    if request.method == "GET":
        quotes_list = Quote.objects.all()
        paginator = MyPagePagination()
        result_page = paginator.paginate_queryset(quotes_list, request)
        quotes_dict = QuoteSerializer(result_page, many=True)
        return Response(quotes_dict.data)
    if request.method == "POST":
        new_instance = request.data
        serializer = QuoteSerializer(data=new_instance)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Added Successfully"})
        return Response({"msg": "Invalid detauils"})


@extend_schema(responses=QuoteSerializer)
@api_view(["GET", "PUT", "DELETE", "PATCH"])
def quote_get_update_delete(request, id):
    try:
        quote_instance = Quote.objects.get(id=id)
    except Quote.DoesNotExist:
        return Response(
            {"Detail": "Record Doesnot Exisist"}, status=status.HTTP_404_NOT_FOUND
        )

    if request.method == "GET":
        serializer = QuoteSerializer(quote_instance)
        return Response(serializer.data)
    if request.method == "PUT" or request.method == "PATCH":
        new_instance = request.data
        serializer = QuoteSerializer(quote_instance, data=new_instance, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Updated Successfully"})
        return Response({"msg": "Invalid Details"})

    if request.method == "DELETE":
        quote_instance.delete()
        return Response({"msg": "Deleted Successfully"})


class CategorieAPI(APIView):
    authentication_classes=[JWTAuthentication]
    permission_classes=[DjangoModelPermissionsOrAnonReadOnly]
    queryset=Categorie.objects.none()

    @extend_schema(request=CategorieSerializer,responses=CategorieSerializer)
    def get(self, request, id=None):
        cat = self.request.query_params.get("category")
        if id != None:
            try:
                categorie_instance = Categorie.objects.get(id=id)
            except Categorie.DoesNotExist:
                return Response(
                    {"Detail": "Record Doesnot Exisist"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            serializer = CategorieSerializer(categorie_instance)
            return Response(serializer.data)
        elif cat != None:
            try:
                categorie_instance = Categorie.objects.get(title=cat)
            except Categorie.DoesNotExist:
                return Response(
                    {"Detail": "Record Doesnot Exisist"},
                    status=status.HTTP_404_NOT_FOUND,
                )
            serializer = CategorieSerializer(categorie_instance)
            return Response(serializer.data)
        categorie_instance = Categorie.objects.all()
        paginator = MyPagePagination()
        result_page = paginator.paginate_queryset(categorie_instance, request)
        serializer = CategorieSerializer(result_page, many=True)
        return Response(serializer.data)

    @extend_schema(request=CategorieSerializer,responses=CategorieSerializer)  
    def post(self, request, format=None):
        print("HEllo")
        new_instance = request.data
        serializer = CategorieSerializer(data=new_instance)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Added Successfully"})
        return Response({"msg": "Invalid detauils"})

    @extend_schema(responses=CategorieSerializer)
    def put(self, request, id, format=None):
        try:
            categorie_instance = Categorie.objects.get(id=id)
        except Categorie.DoesNotExist:
            return Response(
                {"Detail": "Record Doesnot Exisist"}, status=status.HTTP_404_NOT_FOUND
            )
        new_instance = request.data
        serializer = CategorieSerializer(
            categorie_instance, data=new_instance, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Updated Successfully"})
        return Response({"msg": "Invalid Details"})

    @extend_schema(responses=CategorieSerializer)
    def patch(self, request, id, format=None):
        try:
            categorie_instance = Categorie.objects.get(id=id)
        except Categorie.DoesNotExist:
            return Response(
                {"Detail": "Record Doesnot Exisist"}, status=status.HTTP_404_NOT_FOUND
            )
        new_instance = request.data
        serializer = CategorieSerializer(
            categorie_instance, data=new_instance, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Updated Successfully"})
        return Response({"msg": "Invalid Details"})

    @extend_schema(responses=CategorieSerializer)
    def delete(self, request, id, format=None):
        try:
            categorie_instance = Categorie.objects.get(id=id)
        except Categorie.DoesNotExist:
            return Response(
                {"Detail": "Record Doesnot Exisist"}, status=status.HTTP_404_NOT_FOUND
            )
        categorie_instance.delete()
        return Response({"msg": "Deleted Successfully"})


class ProductAPI(
    GenericAPIView,
    ListModelMixin,
    CreateModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    lookup_field = "id"
    pagination_class = MyPagePagination
    filter_backends = [DjangoFilterBackend, OrderingFilter, SearchFilter]
    filterset_fields = ["title", "category__title"]
    ordering_fields = ["created_at"]
    search_fields = ["title", "category__title"]
    authentication_classes=[JWTAuthentication]
    permission_classes=[DjangoModelPermissionsOrAnonReadOnly]
    
    def get(self, request, id=None):
        if id:
            return self.retrieve(request, id)
        else:
            return self.list(request)

    def post(self, request):
        return self.create(request)

    def put(self, request, id):
        return self.update(request, id)

    def delete(self, request, id):
        return self.destroy(request, id)




def FaqAPI(request):
    if request.method=="GET":
        payload=[]
        if cache.get("all_faq"):
            faqs=cache.get("all_faq")
            print("From Cache")
        else:
            faqs=FAQ.objects.all()
            cache.set("all_faq",faqs,30)
            print("From DB")
        faqs_dict = FAQSerializer(faqs, many=True)
        return JsonResponse(faqs_dict.data,safe=False) 



