# from rest_framework.pagination import PageNumberPagination

# class MyPagePagination(PageNumberPagination):
#     page_size=10
#     page_query_param="p"
#     page_size_query_param="size"
#     max_page_size=10


from rest_framework.pagination import LimitOffsetPagination

class MyPagePagination(LimitOffsetPagination):
    default_limit=10
    limit_query_param="mylimit"
    offset_query_param="myoffset"
    max_limit=10