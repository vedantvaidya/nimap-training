from django.shortcuts import render
from .models import *
from django.http import HttpResponse, JsonResponse


def home(request):
    return render(request, "index.html")


def selectrelated(request):
    # books=Book.objects.all()
    books = Book.objects.select_related("author")
    for i in books:
        print(i.author)
    return render(request, "selectrelated.html")


def prefetchrelated(request):
    # all_author = Author.objects.all()
    # for author in all_author:
    #     print(author.name)
    #     print(Book.objects.get(author=author.id))
    # return render(request, "prefetchrelated.html")

    all_author = Author.objects.prefetch_related("book_set")
    for author in all_author:
        print(author.name)
        for book in author.book_set.all():
            print(book.title)
    return render(request,"prefetchrelated.html")



    # SELECT * FROM "firstapp_book" INNER JOIN "firstapp_author" ON ("firstapp_book"."author_id" = "firstapp_author"."id")