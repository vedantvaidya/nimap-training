from django.urls import path
from . import views

urlpatterns = [
    path("", views.home),
    path("selectrelated", views.selectrelated),
    path("prefetchrelated", views.prefetchrelated),
]
