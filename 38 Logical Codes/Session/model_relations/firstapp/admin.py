from django.contrib import admin
from .models import *

class BookAdmin(admin.ModelAdmin):
    list_display=['id','title',"pages",'author']
admin.site.register(Book,BookAdmin)

class AuthorAdmin(admin.ModelAdmin):
    list_display=['id','name',"age"]
admin.site.register(Author,AuthorAdmin)