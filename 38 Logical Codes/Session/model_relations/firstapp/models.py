from django.db import models

class Author(models.Model):
    name=models.CharField(max_length=50)
    age=models.IntegerField()

    def __str__(self):
        return self.name
    
    class Meta:
        db_table="Author"

class Book(models.Model):
    title=models.CharField(max_length=30)
    pages=models.IntegerField()
    author=models.ForeignKey(Author,on_delete=models.CASCADE)

    class Meta:
        db_table="Book"

    def __str__(self):
        return self.title
