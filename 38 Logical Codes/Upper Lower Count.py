a="Hello World!"
upper=0
lower=0
for i in a:
    if ord(i) in range(65,91):
        upper+=1
    elif ord(i) in range(97,123):
        lower+=1
    else:
        continue
print("Upper: ",upper)
print("Lower: ",lower)
