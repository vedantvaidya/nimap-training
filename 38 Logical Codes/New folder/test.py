import csv

def extract_column_data(csv_file, column_name):
    with open(csv_file, 'r') as file:
        csv_reader = csv.DictReader(file)
        column_data = [row[column_name] for row in csv_reader]
        return column_data

# Example usage
csv_file = 'countries.csv'  # Replace with your CSV file path
column_name = 'name'  # Replace with the name of the column you want to extract

column_data = extract_column_data(csv_file, column_name)
print(column_data)