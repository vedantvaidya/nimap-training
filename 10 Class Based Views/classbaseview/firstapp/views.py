from django.shortcuts import render
from django.views.generic import TemplateView,FormView,CreateView,ListView,DeleteView,UpdateView
from .models import Employee
from .forms import Empform
def home(request):
    return render(request,"home.html")

class Java(TemplateView):
    template_name="java.html"

class AddEmp(FormView):
    form_class=Empform
    template_name="addemp.html"

class AddData(CreateView):
    model=Employee
    fields="__all__"
    success_url="/"

class Emplist(ListView):
    model=Employee
    template_name="emplist.html"

class EmpDelete1(DeleteView):
    model=Employee
    success_url='/elist'

class EmpDelete2(DeleteView):
    template_name='delete.html'
    model=Employee
    success_url='/elist'

class Edit(UpdateView):
    model=Employee
    fields="__all__"
    template_name="edit.html"
    success_url='/elist'