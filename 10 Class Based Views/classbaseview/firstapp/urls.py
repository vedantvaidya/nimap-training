from django.urls import path
from . import views as v
from django.views.generic import TemplateView
urlpatterns = [
    path("",v.home),
    path("jv",v.Java.as_view()),
    path("py",TemplateView.as_view(template_name="python.html")),
    path("addemp",v.AddEmp.as_view()),
    path("adddata",v.AddData.as_view()),
    path("elist",v.Emplist.as_view()),
    path("delete1/<int:pk>",v.EmpDelete1.as_view()),
    path("delete2/<int:pk>",v.EmpDelete2.as_view()),
    path('edit/<int:pk>',v.Edit.as_view()),
]
