from django.db import models


class Employee(models.Model):
    name=models.CharField(max_length=30)
    age=models.IntegerField()
    city=models.CharField(max_length=30)
    gender=models.CharField(max_length=30)

    class Meta:
        db_table="employee"

    def __str__(self):
        return self.name


