from django.shortcuts import render
from django.http import HttpResponse
from django.core.cache import cache

# def home(request): 
#     movie= cache.get('movie', 'has_expired')
#     if movie == 'has_expired':
#         cache.set('movie','5 idiots',30)
#         movie=cache.get('movie')
#     return render(request,"index.html", {"movie":movie})

# def home(request):
#     movie=cache.get_or_set("movie",'3 idiots',30)
#     return render(request,"index.html", {"movie":movie})

# def home(request):
#     data={"name":"Vedant","roll":72}
#     # cache.set_many(data,30)
#     movie=cache.get_many(data)
#     return render(request,"index.html", {"movie":movie})


def home(request):
    cache.delete('movie')
    return render(request,"index.html")




