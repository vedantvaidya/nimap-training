from django.db import models

class CaseFlight(models.Model):
    audit_id = models.BigIntegerField(primary_key=True)
    ticketnum_er_number_unique_id_for_cb_taxi_and_meal17 = models.TextField(db_column='TICKETNUM /ER_NUMBER/Unique ID for CB taxi and meal17', blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters. This field type is a guess.

    class Meta:
        managed = False
        db_table = 'Case_Flight'