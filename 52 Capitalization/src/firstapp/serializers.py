from rest_framework import serializers
from firstapp.models import CaseFlight

class CaseFlightSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseFlight
        fields = "__all__"