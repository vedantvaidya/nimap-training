from django.shortcuts import render
from rest_framework.generics import ListAPIView
from firstapp.models import CaseFlight
from firstapp.serializers import CaseFlightSerializer



class SampleListAPIView(ListAPIView):
    queryset= CaseFlight.objects.values()
    serializer_class= CaseFlightSerializer