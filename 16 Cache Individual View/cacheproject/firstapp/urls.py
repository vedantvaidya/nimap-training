from django.urls import path
from . import views
from django.views.decorators.cache import cache_page

urlpatterns=[
    path("",views.home),
    path("contact",cache_page(30)(views.contact)),
]