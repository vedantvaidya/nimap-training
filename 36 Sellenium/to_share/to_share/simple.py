
from email.mime.base import MIMEBase
import eel
import os
#import pynput
import xlrd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#from pynput.mouse import Button,Controller
import smtplib
from email import encoders
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import time
import random
from datetime import datetime as dt

eel.init('web')

####Read Configuration File ####
def read_config_file():
    separator = "="
    keys = {}
    config_data = open("config\settings.properties")
    config_data = config_data.readlines()
    for prop in config_data:
        if separator in prop:
            name,value = prop.split(separator,1)
            keys[name.strip()] = value.strip()
    return keys
#config_keys=read_config_file()   ####calls read_config_file_program
################################


contacts_list=[]
cont_no=1
successful_contact_sent = 0
cont_lst=[]
unsend_contacts = []
msg=""
imgPath=""
docsPath=""
contact_sheet_path=""
log_list = []

chat_box_path = "/html/body/div[1]/div/div/div[5]/div/footer/div[1]/div/span[2]/div/div[2]/div[1]/div/div[1]/p"



def get_excel_path(excel_fileName):
    project_dir_path = os.getcwd()
    file_Name  = str(excel_fileName)
    print(os.path.join(project_dir_path ,"volume" , "excel" , file_Name))
    return os.path.join(project_dir_path , "volume" ,"excel" , file_Name)

def get_images_path(image_fileName):
    print(os.getcwd() ,"volume" , 'images' , str(image_fileName))
    return os.path.join(os.getcwd() , "volume" , 'images' , str(image_fileName))

def get_document_path(doc_fileName):
    print(os.path.join(os.getcwd(), 'docs' , str(doc_fileName)))
    return os.path.join(os.getcwd(), "volume" , 'docs' , str(doc_fileName))

def read_contacts_list(contactSheet):
    print(contactSheet)
    wb = xlrd.open_workbook(contactSheet)
    sheet = wb.sheet_by_index(0)
    no_of_row = sheet.nrows
    print(sheet.nrows)
    contact=[]
    for i in range(sheet.nrows):
        a=sheet.cell_value(i,0)
        print(type(a))
        if isinstance(a, float):
            a=str(int(a))
        if not(not (a and not a.isspace())):
            contact.append(a)
    print(contact)
    return no_of_row,contact


def open_contact(browser,contact):
    # Search by contact name
    #search_box = browser.find_element_by_class_name('_3FRCZ.copyable-text.selectable-text')
    x_path = "/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div[2]/div/div[2]"
    # /html/body/div[1]/div/div/div[3]/div/div[1]/div/label/div/div[2]
    # search_box = browser.find_element(x_path)
    x_path = '//*[@id="side"]/div[1]/div/div/div[2]/div/div[1]'
    search_box = browser.find_element(By.XPATH, x_path)
    
    search_box.click()
    time.sleep(delay()) 
    print(contact) 
    print(type(contact))
    try:
        search_box.clear()
        print('Contact Cleared')
        
        time.sleep(1)
        search_box.send_keys(contact)
        time.sleep(4)
        #_3q9s6
        display_picture_box = browser.find_elements(By.CLASS_NAME,'_13jwn')[0]   #### This isnt working via xpath
        # Working Class Name - _3q9s6 (on 17/09/21) -  Replace line -  browser.find_elements_by_class_name('_3q9s6')[0]    #### This isnt working via xpath
        #_8hzr9 M0JmA i0jNr
        #display_picture_box = browser.find_element_by_xpath("/html/body/div[1]/div/div/div[3]/div/div[2]/div[1]/div/div/div[1]/div/div/div[2]/div[1]/div[1]")
        # display_picture_box = browser.find_elements_by_class_name('TbtXF')[0]
        time.sleep(2)
        # Open the chatbox
        search_box.send_keys(Keys.ARROW_DOWN)
        time.sleep(2) 
        search_box.send_keys(Keys.ENTER)
        time.sleep(2)
        try: 
            chat_box = browser.find_element(By.XPATH,chat_box_path)
            #/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/span[2]/div/div[2]/div[1]/div/div[2]
            #/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]
            # /html/body/div[1]/div/div/div[4]/div/footer/div[1]/div/span[2]/div/div[2]/div[1]/div
            time.sleep(1)
            print(contact)
            return 1
        except NoSuchElementException:
            unsend_contacts.append(contact)
            print('{} contact is blocked or chat box element not found'.format(contact))
            print(unsend_contacts)
            # search_box.click()
            cancel_btn_x_path = '//*[@id="side"]/div[1]/div/div/span/button'
            clear_btn = browser.find_element(By.XPATH, cancel_btn_x_path)
            clear_btn.click()
            return 2
    except Exception as e:
        print(e)
        #if display_picture_box:
        unsend_contacts.append(contact)
        print('{} contact is not saved'.format(contact))
        print(unsend_contacts)
        cancel_btn_x_path = '//*[@id="side"]/div[1]/div/div/span/button'
        clear_btn = browser.find_element(By.XPATH, cancel_btn_x_path)
        clear_btn.click()
        return 3
    
    
def send_message(browser,message):
    message_box = browser.find_element(By.XPATH,chat_box_path)
    #/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]
    #/html/body/div/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div[2]/div/div[1]/div/div[2]
    content = message.split('\n')
    content = ('\n').join(content)
    content = content.split('\n')
    try:
        for msg in content:
            message_box.send_keys(msg)
            message_box.send_keys(Keys.SHIFT,'\n')
        time.sleep(delay())
        message_box.send_keys(Keys.ENTER)
    except Exception as e:
        pass
        # config_keys=read_config_file()
        # print(e)
        # send_email(config_keys,e)

def send_documents(browser,documentPath,contact,i):
    print(documentPath)
    documentPathList = documentPath.split(',')
    print(documentPathList)
    for doc in documentPathList:
        try:
            print(doc)
            documentFileFullPath = get_document_path(doc)
            print(documentFileFullPath)
            # attach=browser.find_element(By.XPATH, '//*[@id="main"]/footer/div[1]/div/span[2]/div/div[1]/div/div/div/div/span')
            attach=browser.find_element(By.CSS_SELECTOR, 'span[data-icon="attach-menu-plus"]') 
            attach.click()
            time.sleep(1)
            # attach_image = browser.find_element(By.XPATH,'//*[@id="main"]/footer/div[1]/div/span[2]/div/div[1]/div/div/span/div/ul/div/div[1]/li/div/span')
            # attach_image.click()
            # time.sleep(2)
            # attachment_box = browser.find_element(By.CSS_SELECTOR, "span[data-icon='clip']",)
            # attachment_box.click()
            # time.sleep(2)
            #document_icon = browser.find_element_by_xpath('/html/body/div[1]/div/div/div[4]/div/footer/div[1]/div[1]/div[2]/div/span/div/div/ul/li[3]/button/span')
            document_icon = browser.find_element(By.CSS_SELECTOR, "input[accept='*']")
            document_icon.send_keys(documentFileFullPath)
            time.sleep(5) ### This much delay, for retrying path for one more time
            send_button = browser.find_element(By.CSS_SELECTOR, "span[data-icon='send']")
            send_button.click()
            time.sleep(2)
            notify="Document sent to {} on {:%Y-%b-%d  %H:%M:%S} and {} remaining contacts".format(contact,dt.now(),cont_no-(i+1))
            log_notifier = "{} \n".format(notify)
            log_list.append(log_notifier)
            eel.notify_js(notify)
            eel.sleep(1.0)
        except Exception as e:
            print("Send Documents Failed =====> ", str(e))
            #### Retrying to send Document for one more time ####
            for i in range(0,1):
                try:
                    print('In Document Sending Second Attempt Block')
                    attachment_box.click() #### This is to un-click the attachment button again
                    time.sleep(2)
                    documentFileFullPath = get_document_path(doc)
                    attachment_box = browser.find_element(By.CSS_SELECTOR, "span[data-icon='clip']")
                    attachment_box.click()
                    time.sleep(3)
                    document_icon = browser.find_element(By.CSS_SELECTOR, "input[accept='*']")
                    document_icon.send_keys(documentFileFullPath)
                    time.sleep(6) ### This much delay, for retrying path for one more time
                    send_button = browser.find_element(By.CSS_SELECTOR, "span[data-icon='send']")
                    send_button.click()
                    time.sleep(2)
                    notify="Document sent to {} on {:%Y-%b-%d  %H:%M:%S} and {} remaining contacts -- Second Attempt".format(contact,dt.now(),cont_no-(i+1))
                    log_notifier = "{} \n".format(notify)
                    log_list.append(log_notifier)
                    eel.notify_js(notify)
                    eel.sleep(1.0)
                    #####
                except Exception as e:
                    print(e)
                    notify_fail_image_message = 'Document with {} this path not found for contact {}'.format(documentFileFullPath,contact)
                    log_notifier = "{} \n".format(notify_fail_image_message)
                    log_list.append(log_notifier)
                    print(notify_fail_image_message)
                    eel.notify_fail(notify_fail_image_message)
                    eel.sleep(1.0)


def send_images(browser,imagePath,contact,i):
    type(imagePath)
    imagePathList=imagePath.split(',')
    print(imagePathList)
    for imageFilePath in imagePathList:
        try:
            imageFileFullPath = get_images_path(imageFilePath)
            #attachment_box = browser.find_elements_by_class_name('_2wfYK')[5]
            # attach=browser.find_element(By.XPATH, '//*[@id="main"]/footer/div[1]/div/span[2]/div/div[1]/div/div/div/div/span')
            attach=browser.find_element(By.CSS_SELECTOR, 'span[data-icon="attach-menu-plus"]')            
            attach.click()
            time.sleep(1)
            # attach_image = browser.find_element(By.XPATH,'//*[@id="main"]/footer/div[1]/div/span[2]/div/div[1]/div/div/span/div/ul/div/div[2]/li/div/span')
            # attach_image.click()
            # time.sleep(2)
            image_box = browser.find_element(By.CSS_SELECTOR, "input[accept='image/*,video/mp4,video/3gpp,video/quicktime']")
            image_box.send_keys(imageFileFullPath)
            
            time.sleep(5)
            send_button = browser.find_element(By.CSS_SELECTOR, "span[data-icon='send']")
            send_button.click()
            time.sleep(2)
            notify="Image sent to {} on {:%Y-%b-%d  %H:%M:%S} and {} remaining contacts".format(contact,dt.now(),cont_no-(i+1))
            log_notifier = "{} \n".format(notify)
            log_list.append(log_notifier)
            eel.notify_js(notify)
            eel.sleep(1.0)
        except Exception as e:
            print("Send Images Failed =====> ", str(e))
            for i in range(0,1):
                try:
                    attachment_box.click() #### This is to un-click the attachment button again
                    time.sleep(2)
                    imageFileFullPath = get_images_path(imageFilePath)
                    #attachment_box = browser.find_elements_by_class_name('_2wfYK')[5]
                    attachment_box = browser.find_element(By.CSS_SELECTOR, "span[data-icon='clip']")
                    attachment_box.click()
                    time.sleep(2)
                    image_box = browser.find_element(By.CSS_SELECTOR, "input[accept='image/*,video/mp4,video/3gpp,video/quicktime']")
                    image_box.send_keys(imageFileFullPath)
                    time.sleep(5)
                    send_button = browser.find_element(By.CSS_SELECTOR, "span[data-icon='send']")
                    send_button.click()
                    time.sleep(2)
                    notify="Image sent to {} on {:%Y-%b-%d  %H:%M:%S} and {} remaining contacts".format(contact,dt.now(),cont_no-(i+1))
                    log_notifier = "{} \n".format(notify)
                    log_list.append(log_notifier)
                    eel.notify_js(notify)
                    eel.sleep(1.0)
                    #####
                except Exception as e:  
                    print(e)
                    notify_fail_image_message = 'Media with {} this path not found for contact {}'.format(imageFilePath,contact)
                    log_notifier = "{} \n".format(notify_fail_image_message)
                    log_list.append(log_notifier)
                    print(notify_fail_image_message)
                    eel.notify_fail(notify_fail_image_message)
                    eel.sleep(1.0)

@eel.expose
def get_input_data():
    group_name = input("Enter the name of the group > ")
    message = input("Enter the Message > ")
    return group_name,message

def setup():
    driver_path = "drivers/chromedriver"
    browser = webdriver.Chrome(driver_path)
    return browser

def delay():
    hold = random.randint(2,5)
    return hold


browser = setup()
browser.get('https://web.whatsapp.com/')
browser.maximize_window()

@eel.expose
def load_whatsapp(contactSheetName,message,imagePath=None,document_path=None,browser=browser):
    print(contactSheetName,message,imagePath,document_path)
    global msg
    global imgPath
    global docsPath
    global contact_sheet_path
    print(type(imagePath))
    print(type(imgPath))
    contactSheetName,message,imagePath,document_path = contactSheetName,message,imagePath,document_path
    msg = message
    imgPath = imagePath
    docsPath = document_path

    print(imgPath)
    print(docsPath)
    contact_sheet_path = contactSheetName
    print(contact_sheet_path)
    print(type(imagePath))
    print(type(imgPath))
    print('Doc path type',type(docsPath))
    time.sleep(10)
    
    global contacts_list
    global cont_no

    contactSheet = get_excel_path(excel_fileName=contact_sheet_path)
    cont_no,contacts_list= read_contacts_list(contactSheet)
    
    global cont_lst
    #cont_no=no_of_contacts
    cont_lst=contacts_list
    cont_no=len(cont_lst)
    print(contacts_list)
    #print(no_of_contacts)
    return cont_no,contacts_list


@eel.expose
def send_msg(no_of_contacts=cont_no,contacts_list=cont_lst,browser=browser,imagePath=imgPath,documentPath=docsPath,message=msg):
    eel.notify_js("Message Sending Initiated")
    log_notifier = "**** AUTOMATION START **** \n"
    log_list.append(log_notifier)
    global successful_contact_sent
    successful_contact_sent = 0
    # print('successful_contact_sent',successful_contact_sent)
    for i in range(0,cont_no):
        
        contact = cont_lst[i]
        time.sleep(delay())
        #open_contact(browser,contact)
        #time.sleep(delay())
        status = open_contact(browser,contact)
        print('success_contacts',successful_contact_sent,'i--',i)

        if status==1:
            if not(not (msg and not msg.isspace())):
            #Message Validation: If contact is found and message is not blank
                send_message(browser,msg)
                notify="Message sent to {} on {:%Y-%b-%d  %H:%M:%S} and {} remaining contacts".format(contact,dt.now(),cont_no-(i+1))
                successful_contact_sent = successful_contact_sent + 1 
                log_notifier = "{} \n".format(notify)
                log_list.append(log_notifier)
                eel.notify_js(notify)
                eel.sleep(1.0)
            if not(not (imgPath and not imgPath.isspace())):
                print(imgPath)
                print(imagePath)
                # if status == 1:
                send_images(browser,imgPath,contact,i)
                time.sleep(delay())
            if not(not (docsPath and not docsPath.isspace())): 
                #print(documentPath)
                send_documents(browser,docsPath,contact,i)
                time.sleep(delay())
            # if not(not (msg and not msg.isspace())):
            # #Message Validation: If contact is found and message is not blank
            #     send_message(browser,msg)
            #     notify="Message sent to {} on {:%Y-%b-%d  %H:%M:%S} and {} remaining contacts".format(contact,dt.now(),cont_no-(i+1))
            #     eel.notify_js(notify)
            #     eel.sleep(1.0)
        elif status == 2:
            notify_fail_message="Contact {} is blocked".format(contact)
            #successful_contact_sent = successful_contact_sent - 1
            log_notifier = "{} \n".format(notify_fail_message)
            log_list.append(log_notifier)
            eel.notify_fail(notify_fail_message)
            eel.sleep(1.0)
        elif status == 3:
            notify_fail_message="Contact {} is not saved".format(contact)
            #successful_contact_sent = successful_contact_sent - 1
            log_notifier = "{} \n".format(notify_fail_message)
            log_list.append(log_notifier)
            eel.notify_fail(notify_fail_message)
            eel.sleep(1.0)
    
    # config_keys=read_config_file()   ####calls read_config_file_program
    log_file=open("{}_whatsapp_log.txt".format("user"),"w")
    log_file.writelines(log_list)
    log_file.close()
    
    log_notifier = "****AUTOMATION COMPLETE**** \n"
    log_list.append(log_notifier)
    eel.notify_js("Automation Completed")
    # send_email(config_keys,e=None)          ####call send email program
    # eel.notify_js("Automation Completed")

@eel.expose
def terminate(browser=browser):
    log_notifier = "PROGRAM TERMINATED \n"
    log_list.append(log_notifier)
    browser.close()


#eel.start("main.html" , size=(850,620),position=(320,360))


def send_email(config_keys,e):
    sender_email = config_keys['sender_email_id']
    receiver_email = config_keys['receiver_list']
    password = config_keys['sender_email_password']
    sender_list = config_keys['receiver_list'].split(";")
    cc_list = config_keys['cc'].split(";")
    message = MIMEMultipart("alternative")
    message["Subject"] = "WhatsApp Automation Log Report from {} on {}".format(config_keys['whatsapp_account_name'],dt.now().strftime('%d/%m/%Y %H:%M:%S'))
    message["From"] = sender_email
    message["To"] = config_keys['receiver_list']
    message["Cc"] = config_keys['cc']
    recipients = sender_list+cc_list

    # Create the plain-text and HTML version of your message
    text = """\
    WhatsApp Automation Log Report

    Contacts Scanned = {} 

    Messages Sent Successfully = {}

    Message  = {}

    Image Path = {}

    Docs Path = {}

    Any Errors = {}

    This is an auto-generated email. Please do not reply.
    """.format(cont_no,successful_contact_sent,msg,imgPath,docsPath,e)

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)

    # Attach Log File
    current_directory = os.getcwd()
    filename = "{}_whatsapp's_log.txt".format(config_keys['whatsapp_account_name'])
    attachment = open(filename,'rb')
    p = MIMEBase('application','octet-stream')
    p.set_payload((attachment).read())
    encoders.encode_base64(p)
    p.add_header('Content-Disposition','attachment;filename= %s' %filename)
    message.attach(p)
    # Create secure connection with server and send email
    s=smtplib.SMTP('smtp.gmail.com',587)
    s.starttls()
    s.login(sender_email,password)
    text1=message.as_string()
    s.sendmail(sender_email,recipients,text1)
    s.quit()
    # context = ssl.create_default_context()
    # with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
    #     server.login(sender_email, password)
    #     server.sendmail(
    #         sender_email, receiver_email, message.as_string()
    #     )

# config_keys=read_config_file()
# send_email(config_keys)

print(os.getcwd())
eel.start("main.html")
#python -m eel simple.py web config --add-binary "./drivers/chromedriver.exe;./drivers"


