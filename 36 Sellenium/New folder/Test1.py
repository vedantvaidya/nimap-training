from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
import environ
import datetime
from selenium.webdriver.common.by import By


env = environ.Env()

environ.Env.read_env(".env")

EMAIL = env("EMAIL")
PASSWORD = env("PASSWORD")
TO = env("TO")
CC = env("CC")
DRAFT = f"""
Hi, 

Date:{datetime.date.today()}



Time
Task
9:00 to 11:00
Payroll Task
Created a model for Products, Also set the path of media files because All products will have images as well
11:00 to 01:00
Made an api for the Product list. Also if the amount of products in the inventory is less than it wont show up 
1:15 to 2:00
Lunch Break
2:00 to 6:00
There was a problem with JWT authentication. Took a while, but fixed it.



Repository link : https://gitlab.com/vedantvaidya/nimap-training.git
(This repo is of Nimap Training, not the project repo. Whenever I learn new concepts, I apply them in this repo)


Thanks and Regards,
Vedant Vaidya

"""

def setup():
    path = r"chromedriver-win64\chromedriver-win64\chromedriver.exe"
    driver = webdriver.Chrome(path)
    return driver


driver = setup()

driver.get("https://www.google.com/gmail/about/")
driver.maximize_window()
time.sleep(2)
sign_in_button = driver.find_element("xpath", "/html/body/header/div/div/div/a[2]").click()
time.sleep(1)
driver.find_element("xpath", '//*[@id="identifierId"]').send_keys(EMAIL)
time.sleep(1)
driver.find_element("xpath",'//*[@id="identifierNext"]/div/button/span').click()
time.sleep(3)
driver.find_element("xpath",'//*[@id="password"]/div[1]/div/div[1]/input').send_keys(PASSWORD)
driver.find_element("xpath",'//*[@id="passwordNext"]/div/button/span').click()
time.sleep(10)

current_day = datetime.datetime.now().weekday()
if current_day == 0:
    driver.find_element("xpath",'/html/body/div[8]/div[2]/div/div[2]/div[2]/div[1]/div[1]/div/div').click()
    time.sleep(2)
    to=driver.find_element("xpath",'/html/body/div[19]/div/div/div/div[1]/div[3]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[1]/table/tbody/tr[1]/td[2]/div/div/div[1]/div/div[3]/div/div/div/div/div/input')
    to.send_keys(TO)
    time.sleep(2) 
    driver.find_element("xpath",'/html/body/div[18]/div/div/div/div[1]/div[4]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[1]/table/tbody/tr[4]/td[2]/div[2]/span/span/span[1]').click()
    time.sleep(2)
    driver.find_element("xpath","/html/body/div[18]/div/div/div/div[1]/div[4]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[1]/table/tbody/tr[2]/td[2]/div/div/div[1]/div/div[3]/div/div/div/div/div/input").send_keys(CC)
    time.sleep(2)
    driver.find_element("xpath",'/html/body/div[18]/div/div/div/div[1]/div[4]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/form/div[3]/input').send_keys("Daily Status Report")
    time.sleep(2)
    driver.find_element("xpath",'/html/body/div[18]/div/div/div/div[1]/div[4]/div[1]/div[1]/div/div/div/div[3]/div/div/div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div[1]/div[2]/div[3]/div/table/tbody/tr/td[2]/div[2]/div').send_keys(DRAFT)
    time.sleep(30)

else:
    search=driver.find_element("xpath",'//*[@id="gs_lc50"]/input[1]')
    search.send_keys("is:starred subject:(Daily Report) ")
    search.send_keys(Keys.ENTER)
    time.sleep(5)
    first_email = driver.find_element("xpath",'/html/body/div[8]/div[2]/div/div[2]/div[5]/div/div/div/div/div[2]/div/div[1]/div/div[3]/div[5]/div[1]/div/table/tbody/tr')
    first_email.click()
    time.sleep(5)
    body=driver.find_element('xpath',"/html/body/div[8]/div[2]/div/div[2]/div[5]/div/div/div/div/div[2]/div")
    print("Reacjhed Here")
    reply_all_button=driver.find_element(By.XPATH,'//*[@id=":p6"]/text()')
    reply_all_button.click()
    print("Clicked on Reply ALL")
    type_msg=driver.find_element(By.CLASS_NAME,'editable')
    type.sleep(2)
    type_msg.send_keys(DRAFT)
    print("Message Typed")

    time.sleep(51000)




# is:starred subject:(Daily Report) 
# driver.get("https://www.youtube.com/")
# driver.maximize_window()
# time.sleep(5)

# subject_to_find = "Get it now"
# elements_with_text = driver.find_elements(By.XPATH, f"//*[contains(text(), '{subject_to_find}')]")
# elements_with_text[0].click()
# time.sleep(10)