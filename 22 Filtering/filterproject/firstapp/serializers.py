from .models import Student
from rest_framework import serializers

class SttudentSerializer(serializers.ModelSerializer):
    class Meta:
        model= Student
        fields=['id','name','passedby']