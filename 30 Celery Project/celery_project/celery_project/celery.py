from __future__ import absolute_import, unicode_literals
from celery.schedules import crontab
import os

from celery import Celery
from django.conf import settings

project_name="celery_project"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", f"{project_name}.settings")

app = Celery(project_name)
app.conf.enable_utc = False

app.conf.update(timezone="Asia/Kolkata")

app.config_from_object(settings, namespace="CELERY")


app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f"Request: {self.request!r}")


app.conf.beat_schedule = {
    "name_of_your_task": {
        "task": "firstapp.task.send_mail_func",
        "schedule": crontab(hour=16, minute=56),
        # 'args':(1,)
    }
}