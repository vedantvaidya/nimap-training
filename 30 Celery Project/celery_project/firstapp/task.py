from celery import shared_task


@shared_task(bind=True)
def test_func(self):
    # operations
    for i in range(100):
        print(i)
    return "Done"

@shared_task(bind=True)
def send_mail_func(self):
    print("Celery Beat")
    return "Hello"