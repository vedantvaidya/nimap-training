from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
import io 
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.views.decorators.csrf import csrf_exempt
import requests

@csrf_exempt
def clean_data(request):
    r=requests.get("https://coderbyte.com/api/challenges/json/json-cleaning")
    data=r.json()
    cleaned_data={}
    garbages=["","-","N/A"]
    name,education,hobbies={},{},[]

    for i,j in data["name"].items():
        if j not in garbages:
            name[i]=j
    if data["name"] !={}:
        cleaned_data["name"]=name

    if data['age'] not in garbages:
        cleaned_data["age"]=data['age']

    if data['DOB'] not in garbages:
        cleaned_data["DOB"]=data['DOB']

    for i in data['hobbies']:
        if i not in garbages:
            hobbies.append(i)
    if hobbies!=[]:
        cleaned_data['hobbies']=hobbies

    
    for i,j in data["education"].items():
        if j not in garbages:
            education[i]=j
    if data["education"] !={}:
        cleaned_data["education"]=education
    
    return JsonResponse(cleaned_data)

@csrf_exempt
def clean_data2(request):
    r=requests.get("https://mocki.io/v1/10ca320c-2d5d-45a7-897d-8a0463dc006f")
    data=r.json()
    cleaned_data={}

    for i,j in data.items():
        if type(j) is dict:
            cleaned=dict_cleaner(j)
            cleaned_data[i]=cleaned
            
        
        if type(j) is list:
            cleaned=list_cleaner(j)
            cleaned_data[i]=cleaned
            continue

        if (type(j) is str) or (type(j) is int):
            garbages=["","-","N/A"]
            if j not in garbages:
                cleaned_data[i]=cleaned
    
    return JsonResponse(cleaned_data)


def dict_cleaner(a):
    garbages=["","-","N/A"]
    cleaned={}
    for i,j in a.items():
        if type(j) is dict:
            cleaned[i]=dict_cleaner(j)
        else:
            if j not in garbages:
                cleaned[i]=j
    return cleaned

def list_cleaner(a):
    garbages=["","-","N/A"]
    cleaned=[]
    for i in a:
        if i not in garbages:
            cleaned.append(i)
    return(cleaned)

