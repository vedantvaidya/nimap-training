from django.urls import path
from . import views

urlpatterns=[
    path("challenges/json/json-cleaning",views.clean_data),
    path("api/",views.clean_data2),
]