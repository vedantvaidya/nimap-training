from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from .models import ResetPassword
import uuid
from django.views.decorators.csrf import csrf_exempt
from .task import send_mail_task
import datetime
from django.utils import timezone
from passwordreset.settings import FRONTEND_PASSWORD_RESET_URL

def home(request):
    return HttpResponse("Hello")


@csrf_exempt
def resetpassword(request):
    if request.method == "GET":
        email = request.GET.get("email")
        user_instance = User.objects.get(email=email)
        if user_instance:
            key = uuid.uuid4()
            resetpassword_instance = ResetPassword(token=key, user_id=user_instance.id)
            resetpassword_instance.save()
            send_mail_task.delay(f"{FRONTEND_PASSWORD_RESET_URL}?token={key}",email)
            return JsonResponse({"Msg": "A Password Reset Mail Has Been Sent"})
        else:
            JsonResponse({"Error": "User Dosntexisist"})

    if request.method == "POST":
        token = request.GET.get("token")
        password = request.POST.get("password")
        try:
            timelimit = timezone.now() - datetime.timedelta(minutes=10)
            print(timelimit)
            resetpassword_instance = ResetPassword.objects.get(
                generated_at__gte=timelimit,
                token=token
            )
            print(resetpassword_instance.generated_at)
        except ResetPassword.DoesNotExist:
            resetpassword_instance=None
        if resetpassword_instance:
            user_instance = resetpassword_instance.user
            user_instance.password = make_password(password)
            user_instance.save()
            resetpassword_instance.delete()
            return JsonResponse({"Done": "Password Changed"})
        else:
            return JsonResponse({"msg":"Invalid Request"})
