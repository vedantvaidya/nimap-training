from celery import shared_task
from django.core.mail import send_mail
from passwordreset import settings

@shared_task(bind=True)
def send_mail_task(self,msg,email):
    mail_subject = "Password Reset"
    mail_message = f"Please click this link to reset your password \n\n {msg}"
    to_email = email
    send_mail(
            subject=mail_subject,
            message=mail_message,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[to_email],
            fail_silently=False,
        )
    return "Done"
