from django.db import models
from django.contrib.auth.models import User

class ResetPassword(models.Model):
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    token=models.CharField(max_length=10)
    generated_at=models.DateTimeField(auto_now_add=True)
