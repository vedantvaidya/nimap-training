from django.db import models


class StatusType(models.IntegerChoices):
    open = 1, "Open"
    closed = 2, "Closed"


class Book(models.Model):
    name = models.CharField(max_length=100)
    type = models.SmallIntegerField(
        choices = StatusType.choices,
        default = StatusType.open
    )