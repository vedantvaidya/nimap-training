from django.contrib import admin
from .models import *

class TaskAdmin(admin.ModelAdmin):
    list_display=['id','task_name','status','owner_id','created_at','updated_at']
admin.site.register(Task,TaskAdmin)

class SubTaskAdmin(admin.ModelAdmin):
    list_display=['id','subtask_name','task_id','created_at']
admin.site.register(subtask,SubTaskAdmin)