from django.db import models
from django.contrib.auth.models import User

class Status(models.IntegerChoices):
    Complete=1
    InComplete=0


class Task(models.Model):
    task_name=models.CharField(max_length=100)
    status=models.IntegerField(choices=Status.choices)
    owner_id=models.ForeignKey(User,on_delete=models.CASCADE)
    created_at=models.DateField(auto_now=True)
    updated_at=models.DateField(auto_now=True)

    def __str__(self):
        return self.task_name

class subtask(models.Model):
    subtask_name=models.CharField(max_length=100)
    task_id=models.ForeignKey(Task,on_delete=models.CASCADE)
    created_at=models.DateField(auto_now=True)

    