from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password
from .form import *
from django.views import View

class Home(View):
    def get(self,request):
        return render(request,"index.html")

class HandelSignup(View):
    def get(self,request):
        return render(request,"register.html")
        
    def post(self,request):
        name=request.POST.get('name')
        password=request.POST.get('password')
        password = make_password(password)
        user_instance=User(username=name,password=password)
        user_instance.save()
        return redirect('/')


class HandelLogin(View):
    def get(self,request):
        return render(request,"login.html")

    def post(self,request):
        name=request.POST.get('name')
        pswd=request.POST.get('password')
        user=authenticate(request,username=name,password=pswd)
        if user is not None:
            request.session["uid"]=user.id
            login(request,user)
            return redirect("/tasklist")
        else:
            return redirect("/login")
        
class HandelLogout(View):
    def get(self,request):
        logout(request)
        return redirect("/")


class TaskList(View):
    def get(self,request):
        id=request.session.get("uid")
        all_tasks=Task.objects.filter(owner_id=id)
        dict={"task":all_tasks}
        return render(request,"tasklist.html",dict)


class CreateTask(View):
    def get(self,request):
        return render(request, "createtask.html")
    def post(self,request):
        id=request.session.get("uid")
        user_instance=User.objects.get(id=id)
        task_name=request.POST.get("task_name")
        status=int(request.POST.get("status"))
        task_instance=Task()
        task_instance.task_name=task_name
        task_instance.status=status
        task_instance.owner_id=user_instance
        task_instance.save()
        return redirect('/tasklist')
    


class IndividualTask(View):
    def get(self,request,pk):
        task_instance=Task.objects.get(id=pk)
        subtask_instances=subtask.objects.filter(task_id=task_instance.id)
        dict={"task":task_instance,"subtask":subtask_instances}
        return render(request, "updatetask.html",dict)
    def post(self,request,pk):
        id=request.session.get("uid")
        task_name=request.POST.get("task_name")
        status=int(request.POST.get("status"))
        task_instance=Task.objects.get(id=pk)
        task_instance.task_name=task_name
        task_instance.status=status
        task_instance.save()
        return redirect("/tasklist")
    
class AddSubTask(View):
    def post(self,request,pk):
        task_instance=Task.objects.get(id=pk)
        subtask_name=request.POST.get("subtask_name")
        subtask_instance=subtask()
        subtask_instance.subtask_name=subtask_name
        subtask_instance.task_id=task_instance
        subtask_instance.save()
        # return redirect('/tasklist')
        return redirect('/task/'+str(pk))