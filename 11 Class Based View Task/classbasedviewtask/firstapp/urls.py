from django.urls import path
from . import views

urlpatterns=[
    path("",views.Home.as_view()),
    path("signup",views.HandelSignup.as_view()),
    path("login",views.HandelLogin.as_view()),
    path("tasklist",views.TaskList.as_view()),
    path("logout",views.HandelLogout.as_view()),
    path("createtask",views.CreateTask.as_view()),
    path("task/<int:pk>",views.IndividualTask.as_view()),
    path("addsubtask/<int:pk>/",views.AddSubTask.as_view(), name="add_subtask"),
]