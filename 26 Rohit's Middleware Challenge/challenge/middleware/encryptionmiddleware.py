from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import io
from rest_framework.parsers import JSONParser
from django_cryptography.fields import encrypt

class MyMiddleware:

    def __init__(self,get_response):
        self.get_response=get_response
        print("One Time Initialization Class Based Middleware")
        
    
    def __call__(self,request):
        json_data=request.body
        stream=io.BytesIO(json_data)
        data=JSONParser().parse(stream)
        print(data['title'])
        a=encrypt(data['name'])
        print(a)
        response = self.get_response(request)
        print("This is after view Class Based Middleware")
        return response

